#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# XD2RD (or xd2rd )
#
# XML Data To Relational Data
# Automated translation of data from XML to Relational Tables

# --------------------------------------------------
# Copyright (C) 2021  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys as im_sys
import os as im_os

import sqlite3
from sqlite3 import Error

import xml.etree.ElementTree as ET
import json

from collections import namedtuple

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Use of Named Tuples for View/Table/Column name proxies
# as tuples are immutable in Python, these will serve as organised constants

# Thus, what we see here is a combination of
# - a named tuple for each base table/view 
# - named parts in the tuple for the SQL name of the table/view plus for any columns
# for the tables, these will be all the column names - so as to do the CREATE
# for the views it will only be for custom named columns, as otherwise the relevant
# table.column name constant will be used
# For the tables/views, the name is the part "rsn" = recordsetname
# For the columns, there are some stock abbreviations:
# - cln_i = ID as primary key
# - cln_n = Name
# - cln_{something}i = foreign key to an ID
# with the others usually just being a mnemonic or abbreviation of the likely full name

# -----------------------------------------------------
# The "base" definitions / structures are those that will have existence
# even before any data is loaded from XML content
# - there will be a second set of structures that will be generated
# according to the contents of the loaded XML

# == Tables - aka Base

# Databases
xd2rd_ntdef_dbs = namedtuple('xd2rd_ntdef_dbs', 'main otpt')
# main := the internal database name - SQLite default is itself "main" so this should be made the preset
# otpt := the output database name
xd2rd_dbs = xd2rd_ntdef_dbs("main", "output")
# usage therefore will be like
# sql_str = "DROP VIEW IF EXISTS " + xd2rd_dbs.otpt + "." + xd2rd_nt_v_Nodes_NullChild.rsn + " ;" + "\n"

# = Organising tables

# Genera
# - as in collective organisation of schemas
xd2rd_ntdef_g = namedtuple('xd2rd_ntdef_g', 'rsn cln_gi cln_n')
# cln_i := the primary key
# cln_n := a display name for each meta-schema entry
xd2rd_nt_t_g = xd2rd_ntdef_g("xd2rd_T_Genera", "GeneraID", "Name")

# Schema
# - as in XML Schemas, e.g. as per XSD files
xd2rd_ntdef_s = namedtuple('xd2rd_ntdef_s', 'rsn cln_si cln_gi cln_n cln_v cln_d')
# cln_i := the primary key
# cln_gi := foreign key to the primary key of the Genera
# cln_n := a display name for each schema entry
# cln_v := a version string for each schema entry - versions of the same Genera have the same cln_gi
# cln_d := a boolean for whether a schema is derived from the data (rather than quoting one)
xd2rd_nt_t_s = xd2rd_ntdef_s("xd2rd_T_Schema", "SchemaID", "GeneraID", "Name", "Version", "IsDerived" )

# Fount
# - as in a source, mostly this will be a File reference
xd2rd_ntdef_f = namedtuple('xd2rd_ntdef_f', 'rsn cln_fi cln_si cln_n cln_f cln_dts')
# cln_i := the primary key
# cln_si := foreign key to the primary key of the Schema
# cln_n := a display name for each Fount entry
# cln_f := file ref (string) when the fount is for a file
# cln_dts := date time stamp (string) of the creation of this record (NOT of the file) is for later what's new (records) checking
xd2rd_nt_t_f = xd2rd_ntdef_f("xd2rd_T_Fount", "FountID", "SchemaID", "Name", "Fref", "DataTimeStamp" )

# Reload
# - as in regarding loads or reloads from a Fount
xd2rd_ntdef_r = namedtuple('xd2rd_ntdef_r', 'rsn cln_ri cln_si cln_rt cln_fi')
# cln_i := the primary key
# cln_fi := foreign key to the primary key of the Fount
# cln_rt := date time stamp (string) of the creation of this record, is for later what's new (records) checking
xd2rd_nt_t_r = xd2rd_ntdef_r("xd2rd_T_Reload", "ReloadID", "SchemaID", "RunTmstmp", "FountID" )

# = XML Data Storage tables

# Node
# - storage of the XML elements - called Nodes just to be a little distinct from the elements
# much like how a photo or video of a person is not the person
xd2rd_ntdef_n = namedtuple('xd2rd_ntdef_n', 'rsn cln_ri cln_ni cln_oi cln_d cln_t cln_c cln_ps')
# cln_ri := foreign key to the primary key of the Reload
# cln_ni := the primary key - generated during traversal
# cln_oi := foreign key to the primary key of the parent Node 
# cln_d := depth integer = as noted during traversal
# cln_t := tag string = straight from the XML
# cln_c := content in the tag = straight from the XML
# cln_ps := parent/path sequence of tags = space delimited
# - hence this is the current structure
xd2rd_nt_t_n = xd2rd_ntdef_n("xd2rd_T_Node", "ReloadID", "NodeID", "OwnerNodeID", "DepthN", "TagStr", "ConStr", "PrntSeqStr" )

# Attribute
# - storage of XML attributes - notable as something that JSON doesn't have
xd2rd_ntdef_a = namedtuple('xd2rd_ntdef_a', 'rsn cln_ri cln_ni cln_k cln_v')
# cln_ri := foreign key to the primary key of the Reload
# cln_ni := foreign key to the Node
# cln_k := key string = straight from the XML
# cln_v := value string = straight from the XML
xd2rd_nt_t_a = xd2rd_ntdef_a("xd2rd_T_Attribute", "ReloadID", "NodeID", "KeyStr", "ValStr" )

# = XML transformation guidance tables

# these are permanent tables, but will be populated from analysis of the loaded data
# and will be used to guide the generation of data-driven tables, views and inserts
# one is for the table/view level, and one for their columns
# while those matters could all be handled on the fly in Python, having them in tables
# allows for two things:
# - post run inspection and retrospection
# - later as a basis for cross-run coordination

# MetaTable
#  - sketch concept for managing the transition from node+attribute storage into custom tables
xd2rd_ntdef_t = namedtuple('xd2rd_ntdef_t', 'rsn cln_ri cln_mk cln_t2m cln_p2m cln_rs cln_ptn cln_idc cln_l_gbr cln_gbc cln_agg cln_fc cln_fv cln_d')
# cln_ri := foreign key to the primary key of the Reload
# cln_mk := MetaKey
# cln_t2m := the name of the Table To Make
# cln_p2m := the name of the Pivot View To Make
# cln_rs := which resource set this comes from - where it may as well be the actual view name
# cln_ptn := Parent Table name - for the created table
# cln_l_gbr := list of the columns to be the row GROUP BY in the pivot view
# cln_gbc := the column that will have the values that will pivot into generated columns
# cln_agg := the aggregate expression that will supply values in the generated columns
# cln_fc := Filter_Col
# cln_fv := Filter_Val
# cln_d := Depth - needed to make the tables in the right order
xd2rd_nt_t_t = xd2rd_ntdef_t("xd2rd_T_MetaTable", "ReloadID", \
	"MetaKey", "ToMakeTableNm", "ToMakePvtNm", "SourceRstNm", "PrntTblNm", \
	"RowIdColNm", "GroupByRowsNmLst", "GroupByColNm", "AggExpr", "FilterColNm", "FilterValStr", "DepthN" )

# MetaColumn
# - sketch concept for 
xd2rd_ntdef_c = namedtuple('xd2rd_ntdef_c', 'rsn cln_ri cln_mk cln_tc2m cln_cdt cln_vcvt cln_vcvn')
# cln_ri := foreign key to the primary key of the Reload
# cln_mk := MetaKey
# cln_tc2m := TableColToMake
# cln_cdt := ColDataType
# cln_vcvt := ValueColumnValueText
# cln_vcvn := ValueColumnValueNum
xd2rd_nt_t_c = xd2rd_ntdef_c("xd2rd_T_MetaColumn", "ReloadID", \
	"MetaKey", "ToMakeColNm", "ColDataType", "ValColValTxt", "ValColValNum" )

# Output Table Structure Explanation Tables

# TableIndex
# A table with a row for each created table
xd2rd_ntdef_ti = namedtuple('xd2rd_ntdef_ti', 'rsn cln_ri cln_tn cln_pth cln_tag cln_d cln_pt cln_ptn')
# integer	integer	text	text	text	integer	text
xd2rd_nt_t_ti = xd2rd_ntdef_ti("xd2rd_T_TableIndex", \
	"ReloadID", "ThisTableName", "PrntSeqStr", "TagStr", "DepthN", "PrntTagSeqStr", "PrntTableName" )

# TablePathStep
# A table with a row for each path step to the table, thus a record of the table hierarchy
xd2rd_ntdef_tps = namedtuple('xd2rd_ntdef_tps', 'rsn cln_ri cln_tn cln_sn cln_tag')
# integer	integer	integer	text
xd2rd_nt_t_tps = xd2rd_ntdef_tps("xd2rd_T_TablePathStep", "ReloadID", "TableName", "StepN", "TagStr" )

# TableLink IDEA SCRAPPED integrated into the TableIndex
# A table with a row for each ForeignKey-Reference setting making up the table hierarchy
#xd2rd_ntdef_tl = namedtuple('xd2rd_ntdef_tl', 'rsn cln_ri cln_li cln_ati cln_toi')
# integer	integer	integer	integer
#xd2rd_nt_t_tl = xd2rd_ntdef_tl("xd2rd_T_TableLink", "ReloadID", "LinkID", "AtTableID", "ToTableID" )

# TableLinkDetail
# A table with a row for each column pair in the ForeignKey-Reference connection between two tables.
xd2rd_ntdef_tld = namedtuple('xd2rd_ntdef_tld', 'rsn cln_ri cln_tn cln_atcn cln_tocn')
# integer	integer	text	text
xd2rd_nt_t_tld = xd2rd_ntdef_tld("xd2rd_T_TableLinkDetail", "ReloadID", "TableName", "AtColNm", "ToColNm" )

# == Views 

# -- Meta Prep Views

# These are four views that mirror the structure of the two Meta- tables
# Hence they only need names predefined as the column names will match the tables
# So even though it seems superflous, these will be one-element named tuples
# with just a rsn element
# Their name definitions are here just after the tables they'll mirror
# In actuality these are defined last thing before the generated views

xd2rd_ntdef_MetaTable_Attr = namedtuple('xd2rd_ntdef_MetaTable_Attr', 'rsn')
xd2rd_nt_v_MetaTable_Attr = xd2rd_ntdef_MetaTable_Attr("xd2rd_V_MetaTable_Attr" )

xd2rd_ntdef_MetaColumn_Attr = namedtuple('xd2rd_ntdef_MetaColumn_Attr', 'rsn')
xd2rd_nt_v_MetaColumn_Attr = xd2rd_ntdef_MetaColumn_Attr("xd2rd_V_MetaColumn_Attr" )

xd2rd_ntdef_MetaTable_Uniq = namedtuple('xd2rd_ntdef_MetaTable_Uniq', 'rsn')
xd2rd_nt_v_MetaTable_Uniq = xd2rd_ntdef_MetaTable_Uniq("xd2rd_V_MetaTable_Uniq" )

xd2rd_ntdef_MetaColumn_Uniq = namedtuple('xd2rd_ntdef_MetaColumn_Uniq', 'rsn')
xd2rd_nt_v_MetaColumn_Uniq = xd2rd_ntdef_MetaColumn_Uniq("xd2rd_V_MetaColumn_Uniq" )

# -- Structure Prep Views
# These are four views that mirror the structure of the four Output Structure Info tables
# Hence they only need names predefined as the column names will match the tables

xd2rd_ntdef_View_StrctInfo_TableIndex = namedtuple('xd2rd_ntdef_View_StrctInfo_TableIndex', 'rsn')
xd2rd_nt_v_StrctInfo_TableIndex = xd2rd_ntdef_View_StrctInfo_TableIndex("xd2rd_V_StrctInfo_TableIndex" )

xd2rd_ntdef_View_StrctInfo_TablePathStep = namedtuple('xd2rd_ntdef_View_StrctInfo_TablePathStep', 'rsn')
xd2rd_nt_v_StrctInfo_TablePathStep = xd2rd_ntdef_View_StrctInfo_TablePathStep("xd2rd_V_StrctInfo_TablePathStep" )

xd2rd_ntdef_View_StrctInfo_TableLink = namedtuple('xd2rd_ntdef_View_StrctInfo_TableLink', 'rsn')
xd2rd_nt_v_StrctInfo_TableLink = xd2rd_ntdef_View_StrctInfo_TableLink("xd2rd_V_StrctInfo_TableLink" )

xd2rd_ntdef_View_StrctInfo_TableLinkDetail = namedtuple('xd2rd_ntdef_View_StrctInfo_TableLinkDetail', 'rsn')
xd2rd_nt_v_StrctInfo_TableLinkDetail = xd2rd_ntdef_View_StrctInfo_TableLinkDetail("xd2rd_V_StrctInfo_TableLinkDetail" )

# == Views - stock

# Nodes_Etc
xd2rd_ntdef_Nodes_Etc = namedtuple('xd2rd_ntdef_Nodes_Etc', 'rsn cln_pst')
xd2rd_nt_v_Nodes_Etc = xd2rd_ntdef_Nodes_Etc("xd2rd_V_Nodes_Etc", "PrntTagSeqStr" )

# Attribute Nodes
xd2rd_ntdef_a_n = namedtuple('xd2rd_ntdef_a_n', 'rsn')
xd2rd_nt_v_a_n = xd2rd_ntdef_a_n("xd2rd_V_AttributeNodes" )

# Nodes_NullChild
xd2rd_ntdef_Nodes_NullChild = namedtuple('xd2rd_ntdef_Nodes_NullChild', 'rsn')
xd2rd_nt_v_Nodes_NullChild = xd2rd_ntdef_Nodes_NullChild("xd2rd_V_Nodes_NullChild" )

# Nodes_WhoseTagIsOncePerParent
xd2rd_ntdef_Nodes_WhoseTagIsOncePerParent = namedtuple('xd2rd_ntdef_Nodes_WhoseTagIsOncePerParent', 'rsn')
xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent = xd2rd_ntdef_Nodes_WhoseTagIsOncePerParent("xd2rd_V_Nodes_WhoseTagIsOncePerParent" )

# TablesToMake_Via_Attributes
xd2rd_ntdef_TablesToMake_Via_Attributes = namedtuple('xd2rd_ntdef_TablesToMake_Via_Attributes', 'rsn')
xd2rd_nt_v_TablesToMake_Via_Attributes = xd2rd_ntdef_TablesToMake_Via_Attributes("xd2rd_V_TablesToMake_Via_Attributes" )

# TablesToMake_Via_Nodes_WhoseTagIsOncePerParent
xd2rd_ntdef_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent = namedtuple('xd2rd_ntdef_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent', 'rsn')
xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent = xd2rd_ntdef_Nodes_NullChild("xd2rd_V_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent" )

# TableColumnsToMake_Via_Attibutes
xd2rd_ntdef_TableColumnsToMake_Via_Attibutes = namedtuple('xd2rd_ntdef_TableColumnsToMake_Via_Attibutes', 'rsn')
xd2rd_nt_v_TableColumnsToMake_Via_Attributes = xd2rd_ntdef_TableColumnsToMake_Via_Attibutes("xd2rd_V_TableColumnsToMake_Via_Attributes" )

# TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent
xd2rd_ntdef_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent = namedtuple('xd2rd_ntdef_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent', 'rsn')
xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent = xd2rd_ntdef_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent("xd2rd_V_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent" )

# Nodes_UniqueTagNoChild
xd2rd_ntdef_Nodes_UniqueTagNoChild = namedtuple('xd2rd_ntdef_Nodes_UniqueTagNoChild', 'rsn')
xd2rd_nt_v_Nodes_UniqueTagNoChild = xd2rd_ntdef_Nodes_UniqueTagNoChild("xd2rd_V_Nodes_UniqueTagNoChild" )

# Nodes_UniqueTagNoChild_GB_TagInContext
xd2rd_ntdef_Nodes_UniqueTagNoChild_GB_TagInContext = namedtuple('xd2rd_ntdef_Nodes_UniqueTagNoChild_GB_TagInContext', 'rsn')
xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext = xd2rd_ntdef_Nodes_UniqueTagNoChild_GB_TagInContext("xd2rd_V_Nodes_UniqueTagNoChild_GB_TagInContext" )

# TablesToMake_All_Other_Structures
xd2rd_ntdef_TablesToMake_All_Other_Structures = namedtuple('xd2rd_ntdef_TablesToMake_All_Other_Structures', 'rsn cln_this cln_prnt')
xd2rd_nt_v_TablesToMake_All_Other_Structures = xd2rd_ntdef_TablesToMake_All_Other_Structures("xd2rd_V_TablesToMake_All_Other_Structures", \
	"This_TableName", "Parent_TableName")

# Note: don't need a structure for the columns of the All_Other_Structures tables
# because these all have the same structure apart from their foreign keys 
# which map the connections between them

# TablesToMake_All_Other_Structures
xd2rd_ntdef_AllOtherStructureTables_Instances = namedtuple('xd2rd_ntdef_AllOtherStructureTables_Instances', 'rsn')
xd2rd_nt_v_AllOtherStructureTables_Instances = xd2rd_ntdef_AllOtherStructureTables_Instances("xd2rd_V_TablesToMake_AllOther_Instances" )

# =====================================================
# Other Named Tuples

# MetaCol
# used to make lists of column meta-info for SQL building
xd2rd_ntdef_MetaCol = namedtuple('xd2rd_ntdef_MetaCol', 'tc2m cdt vcvt vcvn')

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Support Funtions

# =====================================================

# generic

def Hack_Log_Clear( pfn, h1_str ):
	if ( h1_str is None ) or ( len(h1_str) == 0 ) :
		h1_str = "Recordsets"
	with open( pfn, "w") as html_log:
		html_log.write( "<h1>" + h1_str + "</h1>" + "\n")

def Hack_Log_Add_Rst( pfn, rst, title, header ):
	# rst := a recordset as returned from an SQL call
	# for SQLite that is a list of rows, where each row is a tuple
	if not im_os.path.exists(pfn):
		open_mode = "w"
	else :
		open_mode = "a"
	with open( pfn, open_mode) as html_log:
		html_str = ""
		if len(title) > 0 :
			html_str += "<h3>" + title + "</h3>\n"
		html_str += '<table border="1" cellpadding="0" cellspacing="0">\n'
		if len( header) > 0 :
			# Create the table's column headers
			html_str += "  <tr>\n"
			for column in header:
				html_str += "    <th>{0}</th>\n".format(column.strip())
			html_str += "  </tr>\n"
		# Create the table's row data
		for row in rst :
			row_as_list = list( row)
			html_str += "  <tr>\n"
			for column in row:
				cell_str = str( column)
				html_str += "    <td>{0}</td>\n".format(cell_str.strip())
			html_str += "  </tr>\n"
		html_str += "</table>"
		html_log.writelines(html_str)

def Hack_Log_Add_Sql( pfn, sql, title ):
	if not im_os.path.exists(pfn):
		open_mode = "w"
	else :
		open_mode = "a"
	with open( pfn, open_mode) as html_log:
		html_str = ""
		if len(title) > 0 :
			html_str += "<h3>" + title + "</h3>\n"
		if len(sql) > 0 :
			html_str += "<pre>" + sql + "</pre>\n"
		html_log.writelines(html_str)

def Hack_Log_Add_RstSql( pfn, rst, sql, title, header ):
	# rst := a recordset as returned from an SQL call
	# for SQLite that is a list of rows, where each row is a tuple
	if not im_os.path.exists(pfn):
		open_mode = "w"
	else :
		open_mode = "a"
	with open( pfn, open_mode) as html_log:
		html_str = ""
		if len(title) > 0 :
			html_str += "<h3>" + title + "</h3>\n"
		if len(sql) > 0 :
			html_str += "<pre>" + sql + "</pre>\n"
		html_str += '<table border="1" cellpadding="0" cellspacing="0">\n'
		if len( header) > 0 :
			# Create the table's column headers
			html_str += "  <tr>\n"
			for column in header:
				html_str += "    <th>{0}</th>\n".format(column.strip())
			html_str += "  </tr>\n"
		# Create the table's row data
		for row in rst :
			row_as_list = list( row)
			html_str += "  <tr>\n"
			for column in row:
				cell_str = str( column)
				html_str += "    <td>{0}</td>\n".format(cell_str.strip())
			html_str += "  </tr>\n"
		html_str += "</table>"
		html_log.writelines(html_str)

# specific for debug

def Hack_Debug_RstLog_FileRef () :
	pfn = im_os.path.join( im_os.path.expanduser('~'), "xd2rd_debug_log_recordsets.html")
	return pfn

def Hack_TblLog_Clear():
	Hack_Log_Clear( Hack_Debug_RstLog_FileRef(), "Recordsets Debug" )

def Hack_TblLog_Add( rst, sql, title, header ):
	Hack_Log_Add_RstSql( Hack_Debug_RstLog_FileRef(), rst, sql, title, header )

def Hack_TblLog_Add_Sql( sql, title ):
	Hack_Log_Add_Sql( Hack_Debug_RstLog_FileRef(), sql, title )

# specific for result

def Hack_Result_TblLog_FileRef () :
	pfn = im_os.path.join( im_os.path.expanduser('~'), "xd2rd_result_recordsets.html")
	return pfn

def Hack_Result_TblLog_Clear():
	Hack_Log_Clear( Hack_Result_TblLog_FileRef(), "Result Recordsets" )

def Hack_Result_TblLog_Add( rst, sql, title, header ):
	Hack_Log_Add_RstSql( Hack_Result_TblLog_FileRef(), rst, sql, title, header )


# =====================================================

# List Recoders

def ConvertTagListToTableName( i_lst_tags ) :
	return " ".join( i_lst_tags )

def DecodeJsonToList( j ):
	# trivial, but more a place to define this 
	# currently does no testing that what we get is just a list
	lst = json.loads( j )
	return lst

def EncodeListToJson( lst ):
	# trival, but more a place to define this 
	j = json.dumps( lst )
	return j

def ConvertTagJsonToTableName( j ) :
	lst = DecodeJsonToList( j )
	s = ConvertTagListToTableName( lst )
	return s

# == SQLite Wrappers

def sqlite_close_connection(conn):
	didok = False
	try:
		if conn:
			conn.close()
			didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok

def sqlite_create_connection_to_file(db_file):
	""" create a database connection to a SQLite database """
	conn = None
	didok = False
	try:
		conn = sqlite3.connect(db_file)
		print("SQLite Version: " + sqlite3.version + " " + db_file)
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_create_connection_in_memory():
	""" create a database connection to a database that resides
		in the memory
	"""
	conn = None;
	didok = False
	try:
		conn = sqlite3.connect(':memory:')
		print("SQLite Version: " + sqlite3.version)
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_create_connections_main_in_memory_output_as_file( outdb_filename, outdb_refname ):
	""" create a database connection to a database that resides
		in the memory
	"""
	conn = None;
	didok = False
	try:
		conn = sqlite3.connect(':memory:')
		print("SQLite Version: " + sqlite3.version)
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	if didok :
		didok = False
		try:
			conn.execute('ATTACH DATABASE ' + outdb_filename + ' AS ' + outdb_refname)
			didok = True
		except Error as e:
			print("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_establish_connection( p_do_file, p_pathfilename ):
	# p_do_file == boolean 
	# p_pathfilename == 
	# this just allows dev-time switching between methods 
	if p_do_file and len( p_pathfilename ) > 0 :
		didok, conn = sqlite_create_connection_to_file( p_pathfilename )
		if didok :
			print( "Connected to " + p_pathfilename )
	else :
		didok, conn = sqlite_create_connection_in_memory()
		if didok :
			print( "Connected to memory database" )
	return didok, conn
	# didok, conn = sqlite_establish_connection()

def sqlite_execute_sql_get_lastrowid( conn, str_sql):
	""" create a table from the create_table_sql statement
	:param conn: Connection object
	:param create_table_sql: a CREATE TABLE statement
	:return:
	"""
	didok = False
	got_lastrowid = 0
	try:
		c = conn.cursor()
		c.execute( str_sql )
		didok = True
		got_lastrowid = c.lastrowid
		conn.commit()
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok, got_lastrowid

def sqlite_execute_sql( conn, str_sql):
	didok, got_lastrowid = sqlite_execute_sql_get_lastrowid( conn, str_sql)
	return didok

def sqlite_fetchwith_sql( conn, str_sql):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		rows = []
	return didok, rows

def sqlite_fetchwith_sql_print_n( conn, str_sql, n_rows):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		rows = []
	if didok :
		# print row of column names
		names = list( map( lambda x: x[0], c.description) )
		# print( "\t".join( names ) )
		# print rows of data
		if False:
			# pending adding data type handling
			n = 1
			while n < n_rows and n <= len(rows) :
				print( "\t".join( rows[n - 1] ) )
	return didok, rows

def sqlite_fetchwith_sql_with_column_names( conn, str_sql):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		rows = []
		names = []
	if didok :
		names = list( map( lambda x: x[0], c.description) )
	return didok, rows, names

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Database Abstraction Layer
# For now, make this just call the SQlite routines
# later this can become an abstraction for handling mulitple data engines

# -----------------------------------------------------
# Some Hack functions for development use - to expose SQL
# submitted to the data engine.
# To be removed eventually

def Hack_Log_FileRef () :
	pfn = im_os.path.join( im_os.path.expanduser('~'), "xd2rd_sql_log.sql")
	# print( pfn)
	return pfn

def Hack_Log_SQL_Clear():
	pfn = Hack_Log_FileRef()
	with open( pfn, "w") as text_log:
		text_log.write( "-- Logging SQL begins" + "\n")

def Hack_Log_SQL_Add( s ):
	pfn = Hack_Log_FileRef()
	if not im_os.path.exists(pfn):
		with open( pfn, "w") as text_log:
			heda_str = X2R_Indx_Make_Header_Row
			text_log.write( s + "\n")
	with open( pfn, "a") as text_log:
		text_log.write( s + "\n")

# -----------------------------------------------------
# DBI = Database Interface functions

def xd2rd_dbi_Connect( p_do_file, p_pathfilename ) :
	didok, conn = sqlite_establish_connection( p_do_file, p_pathfilename )
	if didok :
		Hack_Log_SQL_Clear()
		Hack_TblLog_Clear()
	return didok, conn

def xd2rd_dbi_Execute( conn, str_sql ) :
	didok = sqlite_execute_sql( conn, str_sql)
	if didok :
		Hack_Log_SQL_Add( str_sql )
	return didok

def xd2rd_dbi_Execute_Make( conn, str_sql, objname ) :
	didok = sqlite_execute_sql( conn, str_sql)
	if didok :
		Hack_Log_SQL_Add( str_sql )
	if True:
		Hack_TblLog_Add_Sql( str_sql, objname )
	return didok

def xd2rd_dbi_Fetch( conn, str_sql ) :
	didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	if didok :
		Hack_Log_SQL_Add( str_sql )
		Hack_TblLog_Add( rows, str_sql, "xd2rd_dbi_Fetch", names )
	return didok, rows, names

def xd2rd_dbi_Fetch_Title( conn, str_sql, p_title ) :
	didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	if didok :
		Hack_Log_SQL_Add( str_sql )
		Hack_TblLog_Add( rows, str_sql, p_title, names )
	return didok, rows, names

def xd2rd_dbi_Outlog_Title( conn, str_sql, p_title ) :
	# no return, this just outputs to the recordset log
	didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	if didok :
		Hack_Result_TblLog_Add( rows, str_sql, p_title, names )
	return

def xd2rd_dbi_Fetch_OnlyDataRows( conn, str_sql ) :
	didok, rows = sqlite_fetchwith_sql( conn, str_sql )
	if didok :
		Hack_Log_SQL_Add( str_sql )
		Hack_TblLog_Add( rows, str_sql, "xd2rd_dbi_Fetch_OnlyDataRows", ""  )
	return didok, rows

def xd2rd_dbi_Fetch_Print_N_Rows( conn, str_sql, n_rows ) :
	didok, rows = sqlite_fetchwith_sql_print_n( conn, str_sql, n_rows )
	if didok :
		Hack_Log_SQL_Add( str_sql )
		Hack_TblLog_Add( rows, str_sql, "xd2rd_dbi_Fetch_Print_N_Rows", ""  )
	return didok, rows

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# SQL constructor functions

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

def SqlBld_Strng_RstName_Prefix() :
	return "xd2rd"

def SqlBld_Strng_AppendWithUnderscore( s1, s2) :
	if len( s2) > 0 :
		return s1 + "_" + s2
	else :
		return s1

def SqlBld_ColumnExpr_RstName_Generic( IdRef, InfixStr, SeqRef) :
	# assumes use of stock prefix, so that's not a parameter
	#print( SqlBld_ColumnExpr_RstName_Generic )
	#print( IdRef + " " + InfixStr + " [" + SeqRef + "]")
	sql_line = \
		"( '" + '"' + "' || '" + SqlBld_Strng_RstName_Prefix() + "_' || " + \
		"TRIM( " + IdRef + ")" + \
		" || '_" + InfixStr + " ' || " + \
		SeqRef + " || '" + '"' + "' )"
	#print( sql_line )
	return sql_line

def SqlBld_ColumnExpr_TableName_Attribute( IdExprStr, SeqStr) :
	return SqlBld_ColumnExpr_RstName_Generic( IdExprStr, "TA", SeqStr)

def SqlBld_ColumnExpr_PivotName_Attribute( IdExprStr, SeqStr) :
	return SqlBld_ColumnExpr_RstName_Generic( IdExprStr, "PA", SeqStr)

def SqlBld_ColumnExpr_TableName_UniqueTag( IdExprStr, SeqStr) :
	return SqlBld_ColumnExpr_RstName_Generic( IdExprStr, "TU", SeqStr)

def SqlBld_ColumnExpr_PivotName_UniqueTag( IdExprStr, SeqStr) :
	return SqlBld_ColumnExpr_RstName_Generic( IdExprStr, "PU", SeqStr)

def SqlBld_ColumnExpr_TableName_AllOthers( IdExprStr, SeqStr) :
	return SqlBld_ColumnExpr_RstName_Generic( IdExprStr, "TH", SeqStr)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# X2R Data Setup 

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Make the base tables
# Where base means the structures defined before any data is loaded

# Genera

def xd2rd_setup_table_base_genera( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_g.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_g.cln_gi + " integer PRIMARY KEY, " + "\n" + \
		"  " + xd2rd_nt_t_g.cln_n + " text " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_g.rsn )
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Schema

def xd2rd_setup_table_base_schm( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_s.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_si + " integer PRIMARY KEY, " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_gi + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_n + " text, " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_v + " text, " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_d + " integer " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_s.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Fount

def xd2rd_setup_table_base_fount( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_f.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_fi + " integer PRIMARY KEY, " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_si + " integer, " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_n + " text, " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_f + " text, " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_dts + " text " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_f.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Reload

def xd2rd_setup_table_base_reload( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_r.rsn + " " + "\n" + \
		"( " + \
		"  " + xd2rd_nt_t_r.cln_ri + " integer PRIMARY KEY, " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_si + " integer, " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_rt + " text, " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_fi + " integer " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_r.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Node

def xd2rd_setup_table_base_node( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_n.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ni + " integer PRIMARY KEY, " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_oi + " integer, " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_d + " integer, " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_t + " text, " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_c + " text, " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ps + " text " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_n.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Attribute

def xd2rd_setup_table_base_attr( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_a.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_ni + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_k + " text, " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_v + " text, " + "\n" + \
		"  " + "PRIMARY KEY( " + "\n" + \
		"  " + "  " + xd2rd_nt_t_a.cln_ri + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_a.cln_ni + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_a.cln_k + " ) " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_a.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# MetaTable

def xd2rd_setup_table_base_metatable( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_t.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_mk + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_t2m + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_p2m + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_rs + " text , " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ptn + " text , " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_idc + " text , " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_l_gbr + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_gbc + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_agg + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fc + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fv + " text, " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_d + " integer, " + "\n" + \
		"  " + "PRIMARY KEY( " + "\n" + \
		"  " + "  " + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_t.cln_mk + " ) " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_t.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# MetaColumn

def xd2rd_setup_table_base_metacolumn( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_c.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_mk + " text , " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_tc2m + " text , " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_cdt + " text, " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_vcvt + " text, " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_vcvn + " integer , " + "\n" + \
		"  " + "PRIMARY KEY( " + "\n" + \
		"  " + "  " + xd2rd_nt_t_c.cln_ri + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_c.cln_mk + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_c.cln_tc2m + " ) " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_c.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Output Table Structure Explanation Tables

def xd2rd_setup_table_TableIndex( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_ti.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_tn + " text , " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_pth + " text, " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_tag + " text, " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_d + " integer, " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_pt + " text , " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ptn + " text , " + "\n" + \
		"  " + "PRIMARY KEY( " + "\n" + \
		"  " + "  " + xd2rd_nt_t_ti.cln_ri + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_ti.cln_tn + " ) " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_ti.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_setup_table_TablePathStep( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_tps.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_tn + " text , " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_sn + " integer, " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_tag + " text , " + "\n" + \
		"  " + "PRIMARY KEY( " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tps.cln_ri + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tps.cln_tn + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tps.cln_sn + " ) " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_tps.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_setup_table_TableLinkDetail( p_conn ):
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + xd2rd_nt_t_tld.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_tn + " text , " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_atcn + " text, " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_tocn + " text , " + "\n" + \
		"  " + "PRIMARY KEY( " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tld.cln_ri + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tld.cln_tn + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tld.cln_atcn + ", " + "\n" + \
		"  " + "  " + xd2rd_nt_t_tld.cln_tocn + " ) " + "\n" + \
		") ; "
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_t_tld.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok


# Combined setup call

def xd2rd_setup_tables_base( p_conn ):
	ok_arch = xd2rd_setup_table_base_genera( p_conn )
	ok_schm = xd2rd_setup_table_base_schm( p_conn )
	ok_fech = xd2rd_setup_table_base_fount( p_conn )
	ok_run = xd2rd_setup_table_base_reload( p_conn )
	ok_elem = xd2rd_setup_table_base_node( p_conn )
	ok_attr = xd2rd_setup_table_base_attr( p_conn )
	#
	ok_tabla = xd2rd_setup_table_base_metatable( p_conn )
	ok_colum = xd2rd_setup_table_base_metacolumn( p_conn )
	#
	ok_index = xd2rd_setup_table_TableIndex( p_conn )
	ok_pstep = xd2rd_setup_table_TablePathStep( p_conn )
	#ok_tabla = xd2rd_setup_table_TableLink( p_conn )
	ok_dtail = xd2rd_setup_table_TableLinkDetail( p_conn )
	return ok_arch and ok_schm and ok_fech and ok_run and ok_elem and ok_attr \
		and ok_tabla and ok_colum \
		and ok_index and ok_pstep and ok_dtail

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Make the stock views
# Where stock means the structures defined before any data is loaded

# make a view of attributes and their nodes
# will be used for the pivots

def xd2rd_define_view_nodes_etc( p_conn ):
	print("xd2rd_define_view_nodes_etc" + ":" + xd2rd_nt_v_Nodes_Etc.rsn )
	# add extra columns to the Nodes table, as a single point definition
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_Nodes_Etc.rsn + " ;" + "\n"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_c + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  TRIM( N." + xd2rd_nt_t_n.cln_ps + " || ' ' || " + "N." + xd2rd_nt_t_n.cln_t + " ) AS " + xd2rd_nt_v_Nodes_Etc.cln_pst + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_t_n.rsn + " AS N " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_Nodes_Etc.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# make a view of attributes and their nodes
# will be used for the pivots

def xd2rd_define_view_attribute_nodes( p_conn ):
	print("xd2rd_define_view_attribute_nodes")
	# currently provding all columns from the Nodes table, optimise later removing unneeded
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_a_n.rsn + " ;" + "\n"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_a_n.rsn + " AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_c + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  N." + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"  A." + xd2rd_nt_t_a.cln_k + ", " + "\n" + \
		"  A." + xd2rd_nt_t_a.cln_v + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS N " + "\n" + \
		"  INNER JOIN " + "\n" + \
		"  " + xd2rd_nt_t_a.rsn + " AS A " + "\n" + \
		"    ON " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ri + " = " + " A." + xd2rd_nt_t_a.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ni + " = " + " A." + xd2rd_nt_t_a.cln_ni + " ) " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_a_n.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Make the views leading to the three lists of tables to make

# VIEW: Which nodes have no child nodes
# these can then be considered for treatment like Attributes
# - technically there's no need for the GROUP BY as row muliplication only occurs 
#   for the non-null joins which then get filtered out, but it's a useful clarity

def xd2rd_define_view_Nodes_NullChild( p_conn ):
	print("xd2rd_define_view_Nodes_NullChild")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_Nodes_NullChild.rsn + " ;" + "\n"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_Nodes_NullChild.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_t_n.rsn + " AS N " + "\n" + \
		"  LEFT OUTER JOIN " + "\n" + \
		"  " + xd2rd_nt_t_n.rsn + " AS N_1 " + "\n" + \
		"    ON " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ri + " = " + " N_1." + xd2rd_nt_t_n.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ni + " = " + " N_1." + xd2rd_nt_t_n.cln_oi + " ) " + "\n" + \
		"WHERE " + "\n" + \
		"  N_1." + xd2rd_nt_t_n.cln_ni + " IS NULL " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + " " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_Nodes_NullChild.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok


# VIEW: Nodes where the Tag occurs uniquely per parent Node
# These can be treated similarly to the Attributes

def xd2rd_define_view_Nodes_WhoseTagIsOncePerParent( p_conn ):
	print("xd2rd_define_view_Nodes_WhoseTagIsOncePerParent")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_ni + " ) AS " + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_c + " ) AS " + xd2rd_nt_t_n.cln_c + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_ps + " ) AS " + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_d + " ) AS " + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_v_Nodes_Etc.cln_pst + " ) AS " + xd2rd_nt_v_Nodes_Etc.cln_pst + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS N " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + " " + "\n" + \
		"HAVING " + "\n" + \
		"  COUNT( N." + xd2rd_nt_t_n.cln_ni + " ) = 1 " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# VIEW: Nodes where the Tag occurs uniquely per parent Node and that has no children
# These will be the base for the nodes to not consider making structures for

def xd2rd_define_view_Nodes_UniqueTagNoChild( p_conn ):
	print("xd2rd_define_view_Nodes_UniqueTagNoChild")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_Nodes_UniqueTagNoChild.rsn + " ;" + "\n"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_Nodes_UniqueTagNoChild.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  UT." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  UT." + xd2rd_nt_t_n.cln_ni + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn + " AS UT " + "\n" + \
		"  INNER JOIN " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_NullChild.rsn + " AS NC " + "\n" + \
		"    ON " + "\n" + \
		"    ( NC." + xd2rd_nt_t_n.cln_ri + " = " + " UT." + xd2rd_nt_t_n.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( NC." + xd2rd_nt_t_n.cln_ni + " = " + " UT." + xd2rd_nt_t_n.cln_ni + " ) " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_Nodes_UniqueTagNoChild.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# ----------------------------------------------
# Table Reductions

# Tables to make for holding the attributes
# - there is a set of tables per Reload

def xd2rd_define_view_TablesToMake_Via_Attributes( p_conn ):
	print("xd2rd_define_view_TablesToMake_Via_Attributes")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_TablesToMake_Via_Attributes.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_TablesToMake_Via_Attributes.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_ps + ") AS " + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_t + ") AS " + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  MAX( N." + xd2rd_nt_t_n.cln_d + ") AS " + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  MAX( " + SqlBld_ColumnExpr_TableName_Attribute( "N." + xd2rd_nt_t_n.cln_ri, "N." + xd2rd_nt_v_Nodes_Etc.cln_pst) \
		+ " ) AS " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  MAX( " + SqlBld_ColumnExpr_TableName_AllOthers( "N." + xd2rd_nt_t_n.cln_ri, "N." + xd2rd_nt_t_n.cln_ps) \
		+ " ) AS " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS N " + "\n" + \
		"  INNER JOIN " + "\n" + \
		"  " + xd2rd_nt_t_a.rsn + " AS A " + "\n" + \
		"    ON " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ri + " = " + " A." + xd2rd_nt_t_a.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ni + " = " + " A." + xd2rd_nt_t_a.cln_ni + " ) " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_v_Nodes_Etc.cln_pst + " " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_TablesToMake_Via_Attributes.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok


# Columns in Tables to make for holding the attributes
# - thus there is a set per table per Reload

def xd2rd_define_view_TableColumnsToMake_Via_Attibutes( p_conn ):
	print("xd2rd_define_view_TableColumnsToMake_Via_Attibutes")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"  A." + xd2rd_nt_t_a.cln_k + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS N " + "\n" + \
		"  INNER JOIN " + "\n" + \
		"  " + xd2rd_nt_t_a.rsn + " AS A " + "\n" + \
		"    ON " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ri + " = " + " A." + xd2rd_nt_t_a.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ni + " = " + " A." + xd2rd_nt_t_a.cln_ni + " ) " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"  A." + xd2rd_nt_t_a.cln_k + " " + "\n" + \
		" ;"
	#print( sql_str )
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn)
	print( didok )
	if not didok :
		print( sql_str )
	return didok

# Tables to make because of unique Tags per parent node
# this is where perhaps the XML or schema could have just used Attributes rather than tags
# - as usual, a set of these per Reload

def xd2rd_define_view_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent( p_conn ):
	print("xd2rd_define_view_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  MAX( N_W." + xd2rd_nt_v_Nodes_Etc.cln_pst + ") AS "  + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"  MAX( N_W." + xd2rd_nt_t_n.cln_t + ") AS "  + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  MAX( N_W." + xd2rd_nt_t_n.cln_d + ") AS "  + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  MAX( " + SqlBld_ColumnExpr_TableName_UniqueTag( "N_W." + xd2rd_nt_t_n.cln_ri, "N_W." + xd2rd_nt_v_Nodes_Etc.cln_pst) \
		+ " ) AS " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  MAX( " + SqlBld_ColumnExpr_TableName_AllOthers( "N_W." + xd2rd_nt_t_n.cln_ri, "N_W." + xd2rd_nt_t_n.cln_ps) \
		+ " ) AS " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn + " AS N_W " + "\n" + \
		"  INNER JOIN " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_NullChild.rsn + " AS N_C " + "\n" + \
		"    ON " + "\n" + \
		"    ( N_W." + xd2rd_nt_t_n.cln_ri + " = " + " N_C." + xd2rd_nt_t_n.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N_W." + xd2rd_nt_t_n.cln_ni + " = " + " N_C." + xd2rd_nt_t_n.cln_ni + " ) " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ps + " " + "\n" + \
		"HAVING " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ps + " IS NOT NULL" + "\n" + \
		" ;"
	#print( sql_str )
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Columns in Tables to make because of unique Tags per parent node
# - as usual, a set of these per Table per Reload
# Note: the IS NOT NULL filter is there to skip the root tag - i.e. XML itself

def xd2rd_define_view_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent( p_conn ):
	print("xd2rd_define_view_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_t + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn + " AS N_W " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_t + " " + "\n" + \
		"HAVING " + "\n" + \
		"  N_W." + xd2rd_nt_t_n.cln_ps + " IS NOT NULL " + "\n" + \
		" ;"
	#	"  INNER JOIN " + "\n" + \
	#	"  " + xd2rd_nt_v_Nodes_NullChild.rsn + " AS N_N " + "\n" + \
	#	"    ON " + "\n" + \
	#	"    ( N_W." + xd2rd_nt_t_n.cln_ri + " = " + " N_N." + xd2rd_nt_t_n.cln_ri + " ) " + "\n" + \
	#	"    AND " + "\n" + \
	#	"    ( N_W." + xd2rd_nt_t_n.cln_ni + " = " + " N_N." + xd2rd_nt_t_n.cln_ni + " ) " + "\n" + \
	#print( sql_str )
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# All the unique TagInContext sequences per Reload

def xd2rd_define_view_Nodes_UniqueTagNoChild_GB_TagInContext( p_conn ):
	print("xd2rd_define_view_Nodes_UniqueTagNoChild_GB_TagInContext")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  N_E." + xd2rd_nt_v_Nodes_Etc.cln_pst + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS N_E " + "\n" + \
		"  LEFT OUTER JOIN" + "\n" + \
		"  " + xd2rd_nt_v_Nodes_UniqueTagNoChild.rsn + " AS UTNC " + "\n" + \
		"    ON " + "\n" + \
		"    ( N_E." + xd2rd_nt_t_n.cln_ri + " = " + " UTNC." + xd2rd_nt_t_n.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N_E." + xd2rd_nt_t_n.cln_ni + " = " + " UTNC." + xd2rd_nt_t_n.cln_ni + " ) " + "\n" + \
		"WHERE " + "\n" + \
		"  UTNC." + xd2rd_nt_t_n.cln_ni + " IS NULL " + "\n" + \
		"GROUP BY " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  N_E." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  N_E." + xd2rd_nt_v_Nodes_Etc.cln_pst + " " + "\n" + \
		" ;"
	#print( sql_str )
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# All the remaining needs for tables to make
# This is essentially all the unique TagInContext sequences per Reload
# that have not already been accounted for by the two other table methods
# nor by columns of those tables

def xd2rd_define_view_TablesToMake_All_Other_Structures( p_conn ):
	print("xd2rd_define_view_TablesToMake_All_Other_Structures")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  N_G_K." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N_G_K." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  N_G_K." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"  N_G_K." + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"  " +  SqlBld_ColumnExpr_TableName_AllOthers( "N_G_K."  + xd2rd_nt_t_n.cln_ri, "N_G_K." + xd2rd_nt_v_Nodes_Etc.cln_pst ) + \
		" AS " + xd2rd_nt_v_TablesToMake_All_Other_Structures.cln_this + ", " + "\n" + \
		"  ( " + "CASE WHEN NULLIF( " + "N_G_K." + xd2rd_nt_t_n.cln_ps + ", '' ) IS NOT NULL THEN " + \
		SqlBld_ColumnExpr_TableName_AllOthers( "N_G_K."  + xd2rd_nt_t_n.cln_ri, "N_G_K." + xd2rd_nt_t_n.cln_ps ) + \
		" END ) AS " + xd2rd_nt_v_TablesToMake_All_Other_Structures.cln_prnt + ", " + "\n" + \
		"  N_G_K." + xd2rd_nt_t_n.cln_d + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext.rsn + " AS N_G_K " + "\n" + \
		" ;"
	#print( sql_str )
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# We don't need any view to work the columns for those tables as the 
# structure is quite plain

# Now define the four Prep views for insertions into the Meta- tables

# First - for the Attributes
# Part A - the MetaTable

def xd2rd_define_view_MetaTable_Prep_Attributes( p_conn ):
	print("xd2rd_define_view_MetaTable_Prep_Attributes")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_MetaTable_Attr.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_MetaTable_Attr.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  T2MVA."  + xd2rd_nt_t_n.cln_ri + " AS " + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"  'A_' || T2MVA." + xd2rd_nt_v_Nodes_Etc.cln_pst + " AS " + xd2rd_nt_t_t.cln_mk + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_TableName_Attribute( "T2MVA." + xd2rd_nt_t_n.cln_ri, "T2MVA." + xd2rd_nt_v_Nodes_Etc.cln_pst ) + \
		" AS " + xd2rd_nt_t_t.cln_t2m + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_PivotName_Attribute( "T2MVA." + xd2rd_nt_t_n.cln_ri, "T2MVA." + xd2rd_nt_v_Nodes_Etc.cln_pst ) + \
		" AS " + xd2rd_nt_t_t.cln_p2m + ", " + "\n" + \
		"  '" + xd2rd_nt_v_a_n.rsn + "'" + " AS " + xd2rd_nt_t_t.cln_rs + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ptn + " AS " + xd2rd_nt_t_t.cln_ptn + ", " + "\n" + \
		"  '" + xd2rd_nt_t_n.cln_ni + "'" + " AS " + xd2rd_nt_t_t.cln_idc + ", " + "\n" + \
		"  '[" + '"' + xd2rd_nt_t_n.cln_ri + '"' + "," + '"' + xd2rd_nt_t_a.cln_ni + '"' + "]'" + " AS " + xd2rd_nt_t_t.cln_l_gbr + ", " + "\n" + \
		"  '" + xd2rd_nt_t_a.cln_k + "'" + " AS " + xd2rd_nt_t_t.cln_gbc + ", " + "\n" + \
		"  '" + xd2rd_nt_t_a.cln_v + "'" + " AS " + xd2rd_nt_t_t.cln_agg + ", " + "\n" + \
		"  '" + xd2rd_nt_v_Nodes_Etc.cln_pst + "' AS " + xd2rd_nt_t_t.cln_fc + ", " + "\n" + \
		"  T2MVA." + xd2rd_nt_v_Nodes_Etc.cln_pst + " AS " + xd2rd_nt_t_t.cln_fv + ", " + "\n" + \
		"  T2MVA." + xd2rd_nt_t_n.cln_d + " AS " + xd2rd_nt_t_t.cln_d + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_TablesToMake_Via_Attributes.rsn + " AS T2MVA " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_MetaTable_Attr.rsn)
	#print( didok )
	#print( sql_str )
	if not didok :
		print( sql_str )
	return didok

# Part B- the MetaColumn list

def xd2rd_define_view_MetaColumn_Prep_Attributes( p_conn ):
	print("xd2rd_define_view_MetaColumn_Prep_Attributes")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_MetaColumn_Attr.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_MetaColumn_Attr.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  TC2M_VA."  + xd2rd_nt_t_a.cln_ri + " AS " + xd2rd_nt_t_c.cln_ri + ", " + "\n" + \
		"  'A_' || TC2M_VA." + xd2rd_nt_v_Nodes_Etc.cln_pst + " AS " + xd2rd_nt_t_c.cln_mk + ", " + "\n" + \
		"  ( 'xd2rd_Col_' || TC2M_VA." + xd2rd_nt_t_a.cln_k + " )" + " AS " + xd2rd_nt_t_c.cln_tc2m + ", " + "\n" + \
		"  'text'" + " AS " + xd2rd_nt_t_c.cln_cdt + ", " + "\n" + \
		"  TC2M_VA." + xd2rd_nt_t_a.cln_k + " AS " + xd2rd_nt_t_c.cln_vcvt + ", " + "\n" + \
		"  CAST( NULL AS integer) AS " + xd2rd_nt_t_c.cln_vcvn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn + " AS TC2M_VA " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_MetaColumn_Attr.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_define_view_MetaTable_Prep_UniqChildfree( p_conn ):
	print("xd2rd_define_view_MetaTable_Prep_UniqChildfree")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_MetaTable_Uniq.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_MetaTable_Uniq.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  T2M."  + xd2rd_nt_t_n.cln_ri + " AS " + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"  'U_' || T2M." + xd2rd_nt_t_n.cln_ps + " AS " + xd2rd_nt_t_t.cln_mk + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_TableName_UniqueTag( "T2M." + xd2rd_nt_t_n.cln_ri, "T2M." + xd2rd_nt_t_n.cln_ps ) + \
		" AS " + xd2rd_nt_t_t.cln_t2m + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_PivotName_UniqueTag( "T2M." + xd2rd_nt_t_n.cln_ri, "T2M." + xd2rd_nt_t_n.cln_ps ) + \
		" AS " + xd2rd_nt_t_t.cln_p2m + ", " + "\n" + \
		"  '" + xd2rd_nt_v_Nodes_Etc.rsn + "'" + " AS " + xd2rd_nt_t_t.cln_rs + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ptn + " AS " + xd2rd_nt_t_t.cln_ptn + ", " + "\n" + \
		"  '" + xd2rd_nt_t_n.cln_oi + "'" + " AS " + xd2rd_nt_t_t.cln_idc + ", " + "\n" + \
		"  '[" + '"' + xd2rd_nt_t_n.cln_ri + '"' + "," + '"' + xd2rd_nt_t_n.cln_oi + '"' + "]'" + " AS " + xd2rd_nt_t_t.cln_l_gbr + ", " + "\n" + \
		"  '" + xd2rd_nt_t_n.cln_t + "'" + " AS " + xd2rd_nt_t_t.cln_gbc + ", " + "\n" + \
		"  '" + xd2rd_nt_t_n.cln_c + "'" + " AS " + xd2rd_nt_t_t.cln_agg + ", " + "\n" + \
		"  '" + xd2rd_nt_t_n.cln_ps + "' AS " + xd2rd_nt_t_t.cln_fc + ", " + "\n" + \
		"  T2M." + xd2rd_nt_t_n.cln_ps + " AS " + xd2rd_nt_t_t.cln_fv + ", " + "\n" + \
		"  T2M." + xd2rd_nt_t_n.cln_d + " AS " + xd2rd_nt_t_t.cln_d + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " AS T2M " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_MetaTable_Uniq.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_define_view_MetaColumn_Prep_UniqChildfree( p_conn ):
	print("xd2rd_define_view_MetaColumn_Prep_UniqChildfree")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_MetaColumn_Uniq.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_MetaColumn_Uniq.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  TC2M_U."  + xd2rd_nt_t_n.cln_ri + " AS " + xd2rd_nt_t_c.cln_ri + ", " + "\n" + \
		"  'U_' || TC2M_U." + xd2rd_nt_t_n.cln_ps + " AS " + xd2rd_nt_t_c.cln_mk + ", " + "\n" + \
		"  ( 'xd2rd_Col_' || TC2M_U." + xd2rd_nt_t_n.cln_t + " )" + " AS " + xd2rd_nt_t_c.cln_tc2m + ", " + "\n" + \
		"  'text'" + " AS " + xd2rd_nt_t_c.cln_cdt + ", " + "\n" + \
		"  TC2M_U." + xd2rd_nt_t_n.cln_t + " AS " + xd2rd_nt_t_c.cln_vcvt + ", " + "\n" + \
		"  CAST( NULL AS integer) AS " + xd2rd_nt_t_c.cln_vcvn + " " + "\n" + \
		"FROM " + "\n" + "\n" + \
		"  " + xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " AS TC2M_U " + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_MetaColumn_Uniq.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# ----------------------------------
# Structure Info Prep Views

def xd2rd_define_view_StrctInfo_TableIndex( p_conn ):
	print("xd2rd_define_view_StrctInfo_TableIndex")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_StrctInfo_TableIndex.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	# as from the Phase 1 method
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_StrctInfo_TableIndex.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		"  T_H." + xd2rd_nt_t_n.cln_ri + " AS " + xd2rd_nt_t_ti.cln_ri + ", " + "\n" + \
		"  T_H." + xd2rd_nt_v_TablesToMake_All_Other_Structures.cln_this + " AS " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  T_H." + xd2rd_nt_t_n.cln_ps + " AS " + xd2rd_nt_t_ti.cln_pth + ", " + "\n" + \
		"  T_H." + xd2rd_nt_t_n.cln_t + " AS " + xd2rd_nt_t_ti.cln_tag + ", " + "\n" + \
		"  T_H." + xd2rd_nt_t_n.cln_d + " AS " + xd2rd_nt_t_ti.cln_d + ", " + "\n" + \
		"  T_H." + xd2rd_nt_v_Nodes_Etc.cln_pst + " AS " + xd2rd_nt_t_ti.cln_pt + ", " + "\n" + \
		"  T_H." + xd2rd_nt_v_TablesToMake_All_Other_Structures.cln_prnt + " AS " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn + " AS T_H " + "\n" + \
		"UNION " + "\n" + \
		"SELECT " + "\n" + \
		"  TA." + xd2rd_nt_t_n.cln_ri + " AS " + xd2rd_nt_t_ti.cln_ri + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_TableName_Attribute( "TA." + xd2rd_nt_t_n.cln_ri, "TA." + xd2rd_nt_v_Nodes_Etc.cln_pst) \
		+ " AS " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  TA." + xd2rd_nt_t_n.cln_ps + " AS " + xd2rd_nt_t_ti.cln_pth + ", " + "\n" + \
		"  TA." + xd2rd_nt_t_n.cln_t + " AS " + xd2rd_nt_t_ti.cln_tag + ", " + "\n" + \
		"  TA." + xd2rd_nt_t_n.cln_d + " AS " + xd2rd_nt_t_ti.cln_d + ", " + "\n" + \
		"  TA." + xd2rd_nt_v_Nodes_Etc.cln_pst + " AS " + xd2rd_nt_t_ti.cln_pt + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_TableName_AllOthers( "TA." + xd2rd_nt_t_n.cln_ri, "TA." + xd2rd_nt_t_n.cln_ps) \
		+ " AS " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_TablesToMake_Via_Attributes.rsn + " AS TA " + "\n" + \
		"UNION " + "\n" + \
		"SELECT " + "\n" + \
		"  TU." + xd2rd_nt_t_n.cln_ri + " AS " + xd2rd_nt_t_ti.cln_ri + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_TableName_UniqueTag( "TU." + xd2rd_nt_t_n.cln_ri, "TU." + xd2rd_nt_v_Nodes_Etc.cln_pst) \
		+ " AS " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  TU." + xd2rd_nt_t_n.cln_ps + " AS " + xd2rd_nt_t_ti.cln_pth + ", " + "\n" + \
		"  TU." + xd2rd_nt_t_n.cln_t + " AS " + xd2rd_nt_t_ti.cln_tag + ", " + "\n" + \
		"  TU." + xd2rd_nt_t_n.cln_d + " AS " + xd2rd_nt_t_ti.cln_d + ", " + "\n" + \
		"  TU." + xd2rd_nt_v_Nodes_Etc.cln_pst + " AS " + xd2rd_nt_t_ti.cln_pt + ", " + "\n" + \
		"  " + SqlBld_ColumnExpr_TableName_AllOthers( "TU." + xd2rd_nt_t_n.cln_ri, "TU." + xd2rd_nt_t_n.cln_ps) \
		+ " AS " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn + " AS TU " + "\n" + \
		" ;"

	# as from the Phase 2 method Stages 2 (Attribute) and 3 (Unique)
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_StrctInfo_TableIndex.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# 'xd2rd_ntdef_t', 'rsn cln_ri cln_mk cln_t2m cln_p2m cln_rs cln_idc cln_l_gbr cln_gbc cln_agg cln_fc cln_fv cln_d')
# xd2rd_nt_t_t ( "ReloadID", \
#	"MetaKey", "TableToMake", "PvtToMake", "RstSource", "ID_Col", "Lst_GB_Rows", "GB_Col", "Agg_Expr", "Filter_Col", "Filter_Val", "DepthN" )



def xd2rd_define_view_StrctInfo_TablePathStep( p_conn ):
	print("xd2rd_define_view_StrctInfo_TablePathStep")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_StrctInfo_TablePathStep.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_StrctInfo_TablePathStep.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		" AS " + xd2rd_nt_t_tps.cln_ri + ", " + "\n" + \
		" AS " + xd2rd_nt_t_tps.cln_tn + ", " + "\n" + \
		" AS " + xd2rd_nt_t_tps.cln_sn + ", " + "\n" + \
		" AS " + xd2rd_nt_t_tps.cln_tag + " " + "\n" + \
		"FROM " + "\n" + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_StrctInfo_TablePathStep.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_define_view_StrctInfo_TableLinkDetail( p_conn ):
	print("xd2rd_define_view_StrctInfo_TableLinkDetail")
	sql_str = "" + \
		"DROP VIEW IF EXISTS " + xd2rd_nt_v_StrctInfo_TableLinkDetail.rsn + " ;"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	sql_str = "" + \
		"CREATE VIEW " + "\n" + \
		"  "+ xd2rd_nt_v_StrctInfo_TableLinkDetail.rsn + " " + "\n" + \
		"AS " + "\n" + \
		"SELECT " + "\n" + \
		" AS " + xd2rd_nt_t_tld.cln_ri + ", " + "\n" + \
		" AS " + xd2rd_nt_t_tld.cln_tn + ", " + "\n" + \
		" AS " + xd2rd_nt_t_tld.cln_atcn + ", " + "\n" + \
		" AS " + xd2rd_nt_t_tld.cln_tocn + " " + "\n" + \
		"FROM " + "\n" + "\n" + \
		" ;"
	didok = xd2rd_dbi_Execute_Make( p_conn, sql_str, xd2rd_nt_v_StrctInfo_TableLinkDetail.rsn)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok


# =====================================================

def xd2rd_setup_views_base( p_conn ):
	allok = True
	# 
	didok = xd2rd_define_view_nodes_etc( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_nodes_etc" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_attribute_nodes( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_attribute_nodes" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_Nodes_NullChild( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_Nodes_NullChild" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_Nodes_WhoseTagIsOncePerParent( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_Nodes_WhoseTagIsOncePerParent" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_TablesToMake_Via_Attributes( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_TablesToMake_Via_Attributes" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_TableColumnsToMake_Via_Attibutes( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_TableColumnsToMake_Via_Attibutes" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_Nodes_UniqueTagNoChild( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_Nodes_UniqueTagNoChild" )
	allok = allok and didok
	#
	didok = xd2rd_define_view_Nodes_UniqueTagNoChild_GB_TagInContext( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_Nodes_UniqueTagNoChild_GB_TagInContext" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_TablesToMake_All_Other_Structures( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_TablesToMake_All_Other_Structures" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_MetaTable_Prep_Attributes( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_MetaTable_Prep_Attributes" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_MetaColumn_Prep_Attributes( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_MetaColumn_Prep_Attributes" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_MetaTable_Prep_UniqChildfree( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_MetaTable_Prep_UniqChildfree" )
	allok = allok and didok
	# 
	didok = xd2rd_define_view_MetaColumn_Prep_UniqChildfree( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_MetaColumn_Prep_UniqChildfree" )
	allok = allok and didok
	# StructureInfo Table Supply Views
	# NOTE, DURING DEVELOPMENT THESE DO FAIL SO THE allok clause is neutered
	didok = xd2rd_define_view_StrctInfo_TableIndex( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_StrctInfo_TableIndex" )
	#allok = allok and didok
	# 
	didok = xd2rd_define_view_StrctInfo_TablePathStep( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_StrctInfo_TablePathStep" )
	# ignoring for now as yet to be implemented
	#allok = allok and didok
	#
	didok = xd2rd_define_view_StrctInfo_TableLinkDetail( p_conn )
	if not didok :
		print( "FAILED: xd2rd_define_view_StrctInfo_TableLinkDetail" )
	# ignoring for now as yet to be implemented
	#allok = allok and didok
	# 
	return allok

# =====================================================

def xd2rd_setup( p_do_file, p_pathfilename ):
	didok, conn = xd2rd_dbi_Connect( p_do_file, p_pathfilename )
	# create tables
	if conn is not None:
		didok = xd2rd_setup_tables_base( conn )
		if didok :
			didok = xd2rd_setup_views_base( conn )
	else:
		print("Error! cannot create the database connection.")
	return didok, conn

# =====================================================

def xd2rd_Inspect_Rst_Limit( p_conn, rst_name ) :
	# 
	print( "=====================================================" + rst_name )
	print( "Testing " + rst_name )
	v_fetch_sql = "SELECT * FROM " + rst_name + " LIMIT 200 ;" 
	now_ok, gotrows, gotnames = xd2rd_dbi_Fetch_Title( p_conn, v_fetch_sql, rst_name )
	if now_ok :
		print("Columns: " )
		print( gotnames )
		print("Rows: " )
		print( gotrows )
	else :
		print("!!-Failed Selecting Table: " + rst_name )

def Test_Meta_Views( p_conn ) :
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_a_n.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_Nodes_NullChild.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_TablesToMake_Via_Attributes.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext.rsn )
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn )

def xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, p_rst_name, p_reload_id ) :
	# 
	print( "=====================================================" + p_rst_name )
	print( "Testing " + p_rst_name )
	v_fetch_sql = "SELECT * FROM " + p_rst_name + " "
	v_fetch_sql = v_fetch_sql + "WHERE " + xd2rd_nt_t_n.cln_ri + " = " + str( p_reload_id ) + " "
	v_fetch_sql = v_fetch_sql + "LIMIT 200 ;" 
	now_ok, gotrows, gotnames = xd2rd_dbi_Fetch_Title( p_conn, v_fetch_sql, p_rst_name)
	if now_ok :
		print("Columns: " )
		print( gotnames )
		print("Rows: " )
		print( gotrows )
	else :
		print("!!-Failed Selecting Table: " + p_rst_name )
	Hack_TblLog_Add( gotrows, "", p_rst_name, gotnames )


def Inspect_RecordSets_SpecificReload( p_conn, p_reload_id ) :
	# Inspect the Load Tables
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_t_n.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_t_a.rsn, p_reload_id )
	# Inspect the stock Views
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_Nodes_Etc.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_a_n.rsn, p_reload_id )
	# Inspect the analytical Views
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_Nodes_NullChild.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_Nodes_WhoseTagIsOncePerParent.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_Nodes_UniqueTagNoChild.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_Nodes_UniqueTagNoChild_GB_TagInContext.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_TablesToMake_Via_Attributes.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn, p_reload_id )

def Inspect_MetaRecordSets_SpecificReload_Virtual( p_conn, p_reload_id ) :
	# These are for use after the MetaTable and MetaColumn tables have been populated
	# Inspect the Structure Info Views - will be used to populate the Structure Info  tables
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_StrctInfo_TableIndex.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_StrctInfo_TablePathStep.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_StrctInfo_TableLink.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_StrctInfo_TableLinkDetail.rsn, p_reload_id )

def Inspect_MetaRecordSets_SpecificReload_Actual( p_conn, p_reload_id ) :
	# Inspect the Structure Info tables
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_t_ti.rsn, p_reload_id )
	# xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_t_tps.rsn, p_reload_id )
	#xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_t_tl.rsn, p_reload_id )
	# xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_t_tld.rsn, p_reload_id )

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Provide Data Insertion Calls
# These allow Python functions calls for populating the base tables

def Sq_Str( p_str ):
	return "'" + p_str + "'"

def xd2rd_insert_genera( p_conn, p_i, p_n ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_g.rsn + " " + "\n" + \
		"( " + \
		"  " + xd2rd_nt_t_g.cln_i + ", " + "\n" + \
		"  " + xd2rd_nt_t_g.cln_n + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + Sq_Str( p_n ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_insert_schm( p_conn, p_i, p_mi, p_n, p_v, p_d ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_s.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_i + ", " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_gi + ", " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_n + ", " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_v + ", " + "\n" + \
		"  " + xd2rd_nt_t_s.cln_d + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + str( p_mi ) + " , " + "\n" + \
		"  " + Sq_Str( p_n ) + ", " + "\n" + \
		"  " + Sq_Str( p_v ) + " ," + "\n" + \
		"  " + str( p_d ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_insert_fount( p_conn, p_i, p_si, p_n, p_f, p_dts ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_f.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_i + ", " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_si + ", " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_n + ", " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_f + ", " + "\n" + \
		"  " + xd2rd_nt_t_f.cln_dts + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + str( p_si ) + " , " + "\n" + \
		"  " + Sq_Str( p_n ) + ", " + "\n" + \
		"  " + Sq_Str( p_f ) + ", " + "\n" + \
		"  " + Sq_Str( p_dts ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_insert_reload( p_conn, p_i, p_si, p_rt, p_fi ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_r.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_i + ", " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_si + ", " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_rt + ", " + "\n" + \
		"  " + xd2rd_nt_t_r.cln_fi + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + str( p_si ) + " , " + "\n" + \
		"  " + Sq_Str( p_rt ) + ", " + "\n" + \
		"  " + str( p_fi ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_insert_node( p_conn, p_ri, p_i, p_oi, p_d, p_t, p_c, p_ps ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_n.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_oi + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_d + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_c + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ps + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_ri ) + " , " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + str( p_oi ) + " , " "\n" + \
		"  " + str( p_d ) + " , " + "\n" + \
		"  " + Sq_Str( p_t ) + " , " + "\n" + \
		"  " + Sq_Str( p_c ) + " , " + "\n" + \
		"  " + Sq_Str( p_ps ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	# print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_insert_attr( p_conn, p_ri, p_ni, p_k, p_v ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_a.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_ni + ", " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_k + ", " + "\n" + \
		"  " + xd2rd_nt_t_a.cln_v + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_ri ) + " , " + "\n" + \
		"  " + str( p_ni ) + " , " + "\n" + \
		"  " + Sq_Str( p_k ) + " , " + "\n" + \
		"  " + Sq_Str( p_v ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	# print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# Dummy manual insertions for the metatable and metacolumn tables
# In practice these will be populated by relational INSERT not manually 

def xd2rd_insert_metatable( p_conn, p_ri, p_i, p_ps, p_n, p_fti, p_m ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_t.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_i + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ps + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_n + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fti + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_m + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_ri ) + " , " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + Sq_Str( p_ps ) + " , " + "\n" + \
		"  " + Sq_Str( p_n ) + " , " + "\n" + \
		"  " + str( p_fti ) + " , " + "\n" + \
		"  " + Sq_Str( p_m ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	# print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

def xd2rd_insert_metacolumn( p_conn, p_ri, p_i, p_ti, p_n, p_m ):
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_c.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_i + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_ti + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_n + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_m + " " + "\n" + \
		"  ) " + "\n" + \
		"VALUES( " + "\n" + \
		"  " + str( p_ri ) + " , " + "\n" + \
		"  " + str( p_i ) + " , " + "\n" + \
		"  " + str( p_ti ) + " , " + "\n" + \
		"  " + Sq_Str( p_n ) + " , " + "\n" + \
		"  " + Sq_Str( p_m ) + " " + "\n" + \
		"  ) " + "\n" + \
		";"
	# print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )
	return didok

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Pivot Views

# Make generic code for generating Pivot views
# This the vital facility for transforming rows into columns
# In some SQL dialects, this is available as a straightforward way ,but
# as SQLite does not offer this, a set of CASE expressions needs to be 
# built for each required variation. Hence the need for a Python function.

# Note: this section is not currently being used but is here for reference
# pending its placement elsewhere.
# It uses live fetching to ascertain the values to become the generated columns.
# XD2RD doesn't need this as it has already done the equivalent analysis
# in its own GROUP BY views of the imported XML. Hence, it instead has a 
# simplified version that gets passed the set of columns to generate.

def PvtVw_Live_MakeColNameForColValue( p_cv, p_prefix, p_cvn, p_lst_cn_sofar) :
	# didok :: were we able to do this
	# p_cv :: value to work with, essentially as fetched from the GROUP BY inspection
	# p_cvn :: value's place n in list - used for problem column names
	# p_lst_cn_sofar :: list of column names already set
	# vn :: value name - conversion of p_cv into something we can use in a column name
	# vs :: value string - conversion of p_cv into something we can use in a CASE expression
	# vbad :: value string - just the non-alphanumeric characters
	didok = True # presume ok and just negate this for unresolvable problems
	if isinstance( p_cv, int) :
		vn = str( p_cv ).strip()
		vs = vn
	if isinstance( p_cv, str) :
		if False:
			# remove any+all whitespace values - space, tab, newline, CRLF
			vn = p_cv.strip().translate(str.maketrans('', '', ' \n\t\r'))
		else :
			# rebuild using only alphanumeric values or underscore
			vn = "".join( [character for character in p_cv if (character.isalnum() or character == "_") ] )
		if len(vn) == 0 :
			# have another go
			vbad = "".join( [character for character in p_cv if not (character.isalnum() or character == "_") ] )
			if len( vbad) < 10 :
				vn = "".join( [ ( "u"+str(ord(character)).strip() ) for character in p_cv ] )
		vs = Sq_Str(p_cv)
	else :
		didok = False
		vn  = ""
		vs  = ""
	if len( vn) > 0 :
		cn = p_prefix + vn
	else :
		cn = p_prefix + str(p_cvn).strip()
	if cn in p_lst_cn_sofar :
		# try to make a unique column name
		cn = p_prefix + str(p_cvn).strip()
		if len( vn) > 0 :
			cn = cn + "_" + vn
		didok = not ( cn in p_lst_cn_sofar )
	final_didok = didok and ( len( cn) > 0 ) and ( len( vs) > 0 )
	return final_didok, cn, vs

def PvtVw_Live_GeneratePivotViewSQL( p_conn, p_nm_view, p_nm_table, p_lst_nm_col_gb_row, p_nm_col_gb_col, p_lst_val4col, \
		p_str_where, p_str_expr_agg) :
	# p_nm_view :: name for the new view
	# p_nm_table :: name of the table being analysed
	# p_lst_nm_col_gb_row :: list of column names as pivots rows
	# p_nm_col_gb_col :: name of the column for the pivot columns per discovered values
	# p_str_where :: well formed WHERE clause - applies before the group by
	# p_str_expr_agg :: string with the aggregate expression for pivot row+column places - often just MAX
	print( "PvtVw_Live_GeneratePivotViewSQL" )
	r_didok = False
	r_str_sql = ""
	r_lst_vc = [] # list of the value columns encountered
	if (p_lst_val4col is None) or (len( p_lst_val4col ) == 0 ) :
		# not given a list of values to make columns so lets query the table for that
		# perform a GROUP BY on the single-column pivot
		gb_sql_str = "" + \
			"SELECT " + "\n" + \
			"  " + p_nm_col_gb_col + " " + "\n" + \
			"FROM " + "\n" + \
			"  " + p_nm_table + " " + "\n"
		if len( p_str_where) > 0 :
			gb_sql_str = gb_sql_str + "WHERE " + "\n"
			gb_sql_str = gb_sql_str + p_str_where + " " + "\n"
		gb_sql_str = gb_sql_str + \
			"GROUP BY " + "\n" + \
			"  " + p_nm_col_gb_col + " " + "\n" + \
			";"
		#print( "Made gb_sql_str" )
		#print( gb_sql_str )
		got_ok, gb_rows = xd2rd_dbi_Fetch_OnlyDataRows( p_conn, gb_sql_str)
		# gb_rows is a list of tuples
		lst_col_val = []
		if got_ok :
			#print( "Got Group By of values ok" )
			for row in gb_rows:
				lst_col_val.append( row[0] )
	else :
		lst_col_val = p_lst_val4col
	if len( lst_col_val ) > 0 :
		sql_str = "" + \
			"CREATE VIEW " + "\n" + \
			"  "+ p_nm_view + " " + "\n" + \
			"AS " + "\n" + \
			"SELECT "
		#print( "Building sql_str: " + sql_str )
		lst_cs = [] # list of all columns
		lst_gb = [] # list of the GROUP BY row columns
		# the pivot row columns
		for cn in p_lst_nm_col_gb_row :
			lst_cs.append( "  " + cn + " " )
			lst_gb.append( "  " + cn + " " )
		# the pivot values columns
		i_prefix = "vc_"
		vc_num = 0
		for cv in lst_col_val :
			vc_num = vc_num + 1
			cn_ok, cn, vs = PvtVw_Live_MakeColNameForColValue( cv, i_prefix, vc_num, lst_cs)
			# bypass if we couldn't make names well enough - might need to add sme feedback for if/when this happens
			if cn_ok :
				# use MAX as a default aggregate
				if ( len( p_str_expr_agg) == 0 ) or ( p_str_expr_agg == p_nm_col_gb_col ) :
					i_str_expr_agg = "MAX( " + "" + ")"
				else :
					i_str_expr_agg = p_str_expr_agg
				col_str = "  MAX( CASE WHEN " + p_nm_col_gb_col + " = " + vs + " THEN " + i_str_expr_agg + " END ) AS " + cn + " " + "\n"
				lst_cs.append( col_str ) 
				r_lst_vc.append( cn ) 
		sql_str = sql_str + ", ".join( lst_cs)
		#print( "Building sql_str: " + sql_str )
		sql_str = sql_str + "FROM " + "\n"
		#print( "Building sql_str: " + sql_str )
		sql_str = sql_str + p_nm_table + " "
		#print( "Building sql_str: " + sql_str )
		if len( p_str_where) > 0 :
			sql_str = sql_str + "WHERE " + "\n"
			sql_str = sql_str + p_str_where + " "
		sql_str = sql_str + "GROUP BY " + "\n"
		#print( "Building sql_str: " + sql_str )
		# the pivot row columns
		sql_str = sql_str + ", ".join( lst_gb)
		#print( "Building sql_str: " + sql_str )
		sql_str = sql_str + ";"
		#print( "Building sql_str: " + sql_str )
		r_str_sql = sql_str
		r_didok = True
	else :
		print("Failed xd2rd_dbi_Fetch_OnlyDataRows")
	return r_didok, r_str_sql, r_lst_vc

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Relational Transformations

# These are the features that will be used for data-driven creation
# of structures. Currently these will be done per-Reload
# but later, some high level organisation will also be added
# e.g. recognition of Schemas and Genera

# This will occur for three situations:
# - the hierachial nodes
# - the nodes with unique content child tags
# - the nodes with attributes 

# The hierachial node tables have a similar column structure but vary by
# their foreign key referencing. Thus the generation of these are comparatively 
# straighforward.

# The other two types will require pivot analyses to then feed the creation
# and insertion statement generators. 
 
# =====================================================

# Populate the MetaTable and MetaColumn tables
# this will require three rounds of custom insertions, one each from the pairs of:
# this will require three custom insertions, one each from the pairs of:
# xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn 
# xd2rd_nt_v_TablesToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn & xd2rd_nt_v_TableColumnsToMake_Via_Nodes_WhoseTagIsOncePerParent.rsn
# xd2rd_nt_v_TablesToMake_Via_Attributes.rsn & xd2rd_nt_v_TableColumnsToMake_Via_Attributes.rsn


# Generate the Output Tables From the MetaTable and MetaColumn tables

# Generate the Pivots From the MetaTable and MetaColumn tables

# Generate the Insertions From the MetaTable and MetaColumn tables

# Note that this function does not have a XD2RD prefix as it's a generic SQL generator.
# It does however, rely on knowing the named tuple structure of type xd2rd_ntdef_MetaCol

def PvtVw_FromMeta_GeneratePivotViewSQL( p_nm_view, p_nm_table, p_lst_nm_col_gb_row, p_nm_col_gb_col, p_lst_tpl_MetaCol, \
		p_str_where, p_str_expr_agg) :
	# This is a customised variation of the Pivot View SQL constructor
	# Here it is assumed we get passed all the elements needed to make the SQL
	# p_nm_view :: name for the new view
	# p_nm_table :: name of the table being analysed
	# p_lst_nm_col_gb_row :: list of column names as pivots rows
	# p_nm_col_gb_col :: name of the column for the pivot columns per discovered values
	# p_str_where :: well formed WHERE clause - applies before the group by
	# p_str_expr_agg :: string with the aggregate expression for pivot row+column places - often just MAX
	print( "PvtVw_FromMeta_GeneratePivotViewSQL" )
	r_str_sql = ""
	r_lst_vc = [] # list of the value columns encountered
	if len( p_lst_tpl_MetaCol ) > 0 :
		sql_str = "" + \
			"CREATE VIEW " + "\n" + \
			"  "+ p_nm_view + " " + "\n" + \
			"AS " + "\n" + \
			"SELECT " + "\n"
		#print( "Building sql_str: " + sql_str )
		lst_cs = [] # list of all columns
		lst_gb = [] # list of the GROUP BY row columns
		# the pivot row columns
		for cn in p_lst_nm_col_gb_row :
			lst_cs.append( "  " + cn + " " )
			lst_gb.append( "  " + cn + " " )
		# the pivot values columns
		i_prefix = "vc_"
		vc_num = 0
		for tpl_mc in p_lst_tpl_MetaCol :
			vc_num = vc_num + 1
			# cn_ok, cn, vs = PvtVw_Live_MakeColNameForColValue( cv, i_prefix, vc_num, lst_cs)
			# tc2m cdt vcvt vcvn if i_tpl.cdt == "text":
			cn = tpl_mc.tc2m
			if tpl_mc.cdt == "text":
				vs = "'" + tpl_mc.vcvt + "'"
			else :
				vs = str( tpl_mc.vcvn )
			cn_ok = True
			# bypass if we couldn't make names well enough - might need to add sme feedback for if/when this happens
			if cn_ok :
				# use MAX as a default aggregate
				if ( len( p_str_expr_agg) == 0 ) or ( p_str_expr_agg == p_nm_col_gb_col ) :
					i_str_expr_agg = "MAX( " + "" + ")"
				else :
					i_str_expr_agg = p_str_expr_agg
				col_str = "  MAX( CASE WHEN " + p_nm_col_gb_col + " = " + vs + " THEN " + i_str_expr_agg + " END ) AS " + cn + " "
				lst_cs.append( col_str ) 
				r_lst_vc.append( cn )
		sql_str = sql_str + ", \n".join( lst_cs) + "\n"
		#print( "Building sql_str: " + sql_str )
		sql_str = sql_str + "FROM " + "\n"
		#print( "Building sql_str: " + sql_str )
		sql_str = sql_str + "  " + p_nm_table + " " + "\n"
		#print( "Building sql_str: " + sql_str )
		if len( p_str_where) > 0 :
			sql_str = sql_str + "WHERE " + "\n"
			sql_str = sql_str + "  " + p_str_where + " " + "\n"
		sql_str = sql_str + "GROUP BY " + "\n"
		#print( "Building sql_str: " + sql_str )
		# the pivot row columns
		sql_str = sql_str + ", \n".join( lst_gb)
		#print( "Building sql_str: " + sql_str )
		sql_str = sql_str + ";"
		#print( "Building sql_str: " + sql_str )
		r_str_sql = sql_str
	else :
		print("FAIL: Empty list of column values")
	return r_str_sql

# =====================================================
# Relational Output
# Make constructors for making the tables

# This happens in two phases and three stages
# Phase 1 is a set of very similar tables for the simple hiearchies of XML tags
#   Whle the tables are similar, they have foreign key arrangements that are not the same 
#   Stage 1 is the building and populating of these structures
# Phase 2 uses Pivots to spread data meaningfully across columns
#   Stage 2 is the populating of stock Meta tables, one for Tables, and one for Columns
#     Part 1 of this is the Attributes
#     Part 2 of this is the childless unique tags
#   Stage 3 uses those meta settings to:
#     Part A - generate tables
#     Part B - generate Pivot Views to convert rows to columns 
#     Part C - use the Pivot Views to insert into the tables 
# -----------------------------------------------------

# =====================================================
# Phase 1
# Stage 1

# Data-driven Tables for the hierarchial tag data

# SQL Building Functions - hence these don't perform the actions, just build the SQL

def xd2rd_RO_GenerateSQL_OneHierarchialNodeTable_Creation( p_ThisTableName, p_ParentTableName ):
	print( "xd2rd_RO_GenerateSQL_OneHierarchialNodeTable_Creation" )
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + p_ThisTableName + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ri + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ni + " integer , " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_c + " text, " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_oi + " integer "
	if ( p_ParentTableName is None ) or ( len( p_ParentTableName) == 0 ) :
		sql_str = sql_str + "\n" + "  ) ;"
	else :
		sql_str = sql_str + ", \n" + \
			"  FOREIGN KEY( " + "\n" + \
			"    " + xd2rd_nt_t_n.cln_ri + ", " + "\n"+ \
			"    " + xd2rd_nt_t_n.cln_oi + " ) " + "\n" + \
			"    REFERENCES " + p_ParentTableName + " ( " + "\n" + \
			"      " + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
			"      " + xd2rd_nt_t_n.cln_ni + " ) " + "\n" + \
			"  ) ; "
	return sql_str

def xd2rd_RO_GenerateSQL_OneHierarchialNodeTable_Insertion( p_IntoThisTableName, p_ParentSeq, p_ThisTag ):
	print( "xd2rd_RO_GenerateSQL_OneHierarchialNodeTable_Insertion" )
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + p_IntoThisTableName + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_c + ", " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_oi + " " + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ni + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_c + ", " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_oi + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_Nodes_Etc.rsn + " AS N " + "\n" + \
		"  INNER JOIN " + "\n" + \
		"  " + xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn + " AS T2M ON " + "\n" + \
		"    ( N." + xd2rd_nt_t_n.cln_ri + " = " + " T2M." + xd2rd_nt_t_n.cln_ri + " ) " + "\n" + \
		"    AND " + "\n" + \
		"    ( N." + xd2rd_nt_v_Nodes_Etc.cln_pst + " = " + " T2M." + xd2rd_nt_v_Nodes_Etc.cln_pst + " ) " + "\n" + \
		"WHERE " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_ps + " = " + Sq_Str( p_ParentSeq ) + " " + "\n" + \
		"  AND " + "\n" + \
		"  N." + xd2rd_nt_t_n.cln_t + " = " + Sq_Str( p_ThisTag ) + " " + "\n" + \
		"; "
	return sql_str

def xd2rd_RO_Hierarchial_Generated_Construction( p_conn, p_reload_id, \
		p_lst_ro_tn, p_dct_ro_ct, p_dct_ro_it ):
	# get the set of Hierarchial Node tables to generate
	# because of the foreign key dependencies these are being made by 
	# increasing XML depth with the created SQL actually submitted
	# before carrying on generating more SQL
	print( "xd2rd_RO_Hierarchial_Generated_Construction" )
	v_fetch_sql = "SELECT " + "\n" + \
		"T2M." + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"T2M." + xd2rd_nt_t_n.cln_t + ", " + "\n" + \
		"T2M." + xd2rd_nt_t_n.cln_ps + ", " + "\n" + \
		"T2M." + xd2rd_nt_v_Nodes_Etc.cln_pst + ", " + "\n" + \
		"T2M." + xd2rd_nt_v_TablesToMake_All_Other_Structures.cln_this + ", " + "\n" + \
		"T2M." + xd2rd_nt_v_TablesToMake_All_Other_Structures.cln_prnt + ", " + "\n" + \
		"T2M." + xd2rd_nt_t_n.cln_d + " " + "\n" + \
		"FROM " + xd2rd_nt_v_TablesToMake_All_Other_Structures.rsn + " AS T2M " + "\n" + \
		"WHERE " + "\n" + "  T2M." + xd2rd_nt_t_n.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		"ORDER BY " + "\n" + "  T2M." + xd2rd_nt_t_n.cln_d + "\n" + \
		" ;"
	print( "Fetching:" )
	print( v_fetch_sql )
	now_ok, got_t_rows, got_t_names = xd2rd_dbi_Fetch( p_conn, v_fetch_sql)
	if now_ok :
		print( "Working with rows:" )
		# loop through the rows, each is a table to create
		for rw in got_t_rows :
			print( "Next row:" )
			# rw is a tuple with values in the order governed by the above SELECT
			i_ReloadID = rw[0]
			i_ThisTag = rw[1]
			i_ParentSeq = rw[2]
			i_PrntSeqTag = rw[3]
			i_This_TblNm = rw[4]
			i_Prnt_TblNm = rw[5]
			i_DepthN = rw[6]
			if i_Prnt_TblNm is None :
				i_Prnt_TblNm = ""
			print( "Generating SQL for creating: " + i_This_TblNm )
			tbl_make_sql = xd2rd_RO_GenerateSQL_OneHierarchialNodeTable_Creation( i_This_TblNm, i_Prnt_TblNm )
			print( tbl_make_sql )
			p_lst_ro_tn.append( i_This_TblNm )
			p_dct_ro_ct[ i_This_TblNm ] = tbl_make_sql
			print( "Creating table: " + i_This_TblNm )
			didok = xd2rd_dbi_Execute_Make( p_conn, tbl_make_sql, i_This_TblNm)
			if didok :
				print( "Generating SQL for filling: " + i_This_TblNm )
				tbl_insert_sql = xd2rd_RO_GenerateSQL_OneHierarchialNodeTable_Insertion( i_This_TblNm, i_ParentSeq, i_ThisTag )
				print( tbl_insert_sql )
				p_dct_ro_it[ i_This_TblNm ] = tbl_insert_sql
				print( "Populating table: " + i_This_TblNm )
				didok = xd2rd_dbi_Execute( p_conn, tbl_insert_sql)
				if didok :
					print( "Populated!" )
				else :
					print( "ERROR Not Populated!" )
	print( "END OF " + "xd2rd_RO_Hierarchial_Generated_Construction" )

# =====================================================


# =====================================================
# Phase 2

# =====================================================
# Stage 2
# Populating the Meta Framework tables
# The idea here is mere formalism with the hard work all done in having already
# constructed a pair of suitable views that correspond perfectly to the tables
# into which they insert. Hence a simple function to perform the insert

# cln_ri cln_mk cln_t2m cln_p2m cln_rs cln_l_gbr cln_gbc cln_agg cln_fc cln_fv cln_d
def xd2rd_RO_Populate_Construction_Framework_MetaTable_From_View( p_conn, p_reload_id, p_ViewName ):
	print("xd2rd_RO_Populate_Construction_Framework_MetaTable_From_View")
	#print("Using View: " + p_ViewName )
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_t.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_mk + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_t2m + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_p2m + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_rs + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ptn + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_idc + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_l_gbr + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_gbc + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_agg + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fc + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fv + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_d + " " + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_mk + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_t2m + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_p2m + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_rs + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ptn + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_idc + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_l_gbr + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_gbc + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_agg + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fc + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_fv + ", " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_d + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + p_ViewName + " " + "\n" + \
		"WHERE " + "\n" + \
		"  " + xd2rd_nt_t_t.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		";"
	#print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )

# rsn cln_ri cln_mk cln_tc2m cln_cdt cln_vcvt cln_vcvn
def xd2rd_RO_Populate_Construction_Framework_MetaColumn_From_View( p_conn, p_reload_id, p_ViewName ):
	print("xd2rd_RO_Populate_Construction_Framework_MetaColumn_From_View")
	#print("Using View: " + p_ViewName )
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_c.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_mk + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_tc2m + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_cdt + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_vcvt + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_vcvn + " " + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_mk + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_tc2m + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_cdt + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_vcvt + ", " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_vcvn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + p_ViewName + " " + "\n" + \
		"WHERE " + "\n" + \
		"  " + xd2rd_nt_t_c.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		";"
	#print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )

def xd2rd_RO_Populate_Construction_Framework_For_Attributes( p_conn, p_reload_id ):
	print("xd2rd_RO_Populate_Construction_Framework_For_Attributes")
	xd2rd_RO_Populate_Construction_Framework_MetaTable_From_View( p_conn, p_reload_id, xd2rd_nt_v_MetaTable_Attr.rsn )
	xd2rd_RO_Populate_Construction_Framework_MetaColumn_From_View( p_conn, p_reload_id, xd2rd_nt_v_MetaColumn_Attr.rsn )

def xd2rd_RO_Populate_Construction_Framework_For_UniqueChildfree( p_conn, p_reload_id ):
	print("xd2rd_RO_Populate_Construction_Framework_For_UniqueChildfree")
	xd2rd_RO_Populate_Construction_Framework_MetaTable_From_View( p_conn, p_reload_id, xd2rd_nt_v_MetaTable_Uniq.rsn )
	xd2rd_RO_Populate_Construction_Framework_MetaColumn_From_View( p_conn, p_reload_id, xd2rd_nt_v_MetaColumn_Uniq.rsn )

def xd2rd_RO_Populate_Construction_Framework( p_conn, p_reload_id ):
	print("xd2rd_RO_Populate_Construction_Framework")
	xd2rd_RO_Populate_Construction_Framework_For_Attributes( p_conn, p_reload_id )
	xd2rd_RO_Populate_Construction_Framework_For_UniqueChildfree( p_conn, p_reload_id )

def Inspect_Construction_Framework_SpecificReload( p_conn, p_reload_id ) :
	# Inspect the Generation Control Tables
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_MetaTable_Attr.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_MetaColumn_Attr.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_MetaTable_Uniq.rsn, p_reload_id )
	xd2rd_Inspect_Rst_Limit_SpecificReload( p_conn, xd2rd_nt_v_MetaColumn_Uniq.rsn, p_reload_id )

# =====================================================
# Stage 3

# Now call all the generated table creations and insertions methods

def xd2rd_RO_GenerateSQL_Creation( p_ThisTableName, p_ParentTableName, p_NodeIdColName, p_lst_tpl_MetaCol ):
	print("xd2rd_RO_GenerateSQL_Creation")
	sql_str = "" + \
		"CREATE TABLE IF NOT EXISTS " + "\n" + \
		"  " + p_ThisTableName + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_n.cln_ri + " integer , " + "\n" + \
		"  " + p_NodeIdColName + " integer , " + "\n"
	for nmdtpl in p_lst_tpl_MetaCol :
		sql_str = sql_str + "  " + nmdtpl.tc2m + " " + nmdtpl.cdt + ", " + "\n"
	sql_str = sql_str + \
		"  FOREIGN KEY( " + xd2rd_nt_t_n.cln_ri + ", " + p_NodeIdColName + " ) " + "\n" + \
		"    REFERENCES " + p_ParentTableName + " ( " + "\n" + \
		"      " + xd2rd_nt_t_n.cln_ri + ", " + "\n" + \
		"      " + xd2rd_nt_t_n.cln_ni + " ) " + "\n" + \
		") ; "
	return sql_str

def xd2rd_RO_GenerateSQL_PivotView( p_PivotViewName, p_SourceTableName, p_lst_nm_col_gb_row, p_nm_col_gb_col, \
		p_lst_tpl_MetaCol, p_str_where, p_str_expr_agg ):
	print("xd2rd_RO_GenerateSQL_PivotView")
	# now call the generic non-live-data-check pivot SQL generator
	# sounds fancy but the pivot is really just a big set of case expressions
	sql_str = PvtVw_FromMeta_GeneratePivotViewSQL( p_PivotViewName, p_SourceTableName, \
		p_lst_nm_col_gb_row, p_nm_col_gb_col, p_lst_tpl_MetaCol, \
		p_str_where, p_str_expr_agg)
	return sql_str

def xd2rd_RO_GenerateSQL_Insertion( p_ThisTableName, p_PivotViewName, p_NodeIdColName, p_lst_tpl_MetaCol ):
	print("xd2rd_RO_GenerateSQL_Insertion")
	i_lst_cols = [ xd2rd_nt_t_n.cln_ri, p_NodeIdColName ] 
	for i_tpl in p_lst_tpl_MetaCol :
		i_lst_cols.append( i_tpl.tc2m )
	s_cols = ", \n  ".join( i_lst_cols )
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + p_ThisTableName + " " + "\n" + \
		"( " + "\n" + \
		"  " + s_cols + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  " + s_cols + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + p_PivotViewName + " " + "\n" + \
		"; "
	return sql_str

def xd2rd_RO_Meta_Generated_Construction( p_conn, p_reload_id, \
		p_lst_ro_tn, p_dct_ro_ct, p_dct_ro_pv, p_dct_ro_it ) :
	# the task here is to go through the sets of tables and columns and construct CREATE TABLE sql for each
	print( "xd2rd_RO_Meta_Generated_Construction" )
	#print( "for Reload # " + str( p_reload_id ) )
	# get the set of tables
	# Note: currently selecting all columns just in case - to trim back to just those needed
	v_fetch_sql = "SELECT " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_ri + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_mk + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_t2m + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_p2m + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_rs + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_idc + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_l_gbr + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_gbc + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_agg + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_fc + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_fv + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_d + ", " + "\n" + \
		"M_T." + xd2rd_nt_t_t.cln_ptn + " " + "\n" + \
		"FROM " + xd2rd_nt_t_t.rsn + " AS M_T " + "\n" + \
		"WHERE " + "\n" + "  M_T." + xd2rd_nt_t_t.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		"ORDER BY " + "M_T." + xd2rd_nt_t_t.cln_d + "\n" + \
		" ;"
	print( "Fetching tables:" )
	#print( v_fetch_sql )
	got_t_ok, got_t_rows, got_t_names = xd2rd_dbi_Fetch( p_conn, v_fetch_sql)
	if got_t_ok :
		print( "Working with rows:" )
		# for each table
		for r in got_t_rows :
			print( "Next Meta Table" )
			# to match the SELECT above - so if that changes this must too
			i_cln_ri = r[0]
			i_cln_mk = r[1]
			i_cln_t2m = r[2]
			i_cln_p2m = r[3]
			i_cln_rs = r[4]
			i_cln_idc = r[5]
			i_cln_l_gbr = DecodeJsonToList( r[6] )
			i_cln_gbc = r[7]
			i_cln_agg = r[8]
			i_cln_fc = r[9]
			i_cln_fv = r[10]
			i_cln_d = r[11]
			i_cln_ptn = r[12]
			print( "MetaKey: " + i_cln_mk )
			# get the set of columns
			v_fetch_sql = "SELECT " + "\n" + \
				"M_C." + xd2rd_nt_t_c.cln_tc2m + ", " + "\n" + \
				"M_C." + xd2rd_nt_t_c.cln_cdt + ", " + "\n" + \
				"M_C." + xd2rd_nt_t_c.cln_vcvt + ", " + "\n" + \
				"M_C." + xd2rd_nt_t_c.cln_vcvn + " " + "\n" + \
				"FROM " + xd2rd_nt_t_c.rsn + " AS M_C " + "\n" + \
				"WHERE " + "\n" + \
				"M_C." + xd2rd_nt_t_c.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
				"AND " + "\n" + \
				"M_C." + xd2rd_nt_t_c.cln_mk + " = " + Sq_Str( i_cln_mk ) + " " + "\n" + \
				" ;"
			print( "Fetching columns:" )
			#print( v_fetch_sql )
			got_c_ok, got_t_rows, got_t_names = xd2rd_dbi_Fetch( p_conn, v_fetch_sql)
			if got_c_ok :
				print( "Working with columns:" )
				i_lst_tpl_MetaCol = []
				for r in got_t_rows :
					# tc2m, cdt, vcvt, vcvn
					i_tpl_MetaCol = xd2rd_ntdef_MetaCol( r[0], r[1], r[2], r[3])
					i_lst_tpl_MetaCol.append( i_tpl_MetaCol )
				# generate the CREATE
				sql_create = xd2rd_RO_GenerateSQL_Creation( i_cln_t2m, i_cln_ptn, i_cln_idc, i_lst_tpl_MetaCol )
				create_didok = xd2rd_dbi_Execute_Make( p_conn, sql_create, i_cln_t2m)
				if create_didok :
					p_lst_ro_tn.append( i_cln_t2m)
					p_dct_ro_ct[ i_cln_t2m ] = sql_create
					# generate the PIVOT
					if ( len( i_cln_fc ) > 0 ) and ( len( i_cln_fv ) > 0 ):
						i_str_where  = i_cln_fc + " = " +  "'" + i_cln_fv +  "'"
					else :
						i_str_where = "" 
					i_str_expr_agg = i_cln_agg
					sql_pivotv = xd2rd_RO_GenerateSQL_PivotView( i_cln_p2m, i_cln_rs, i_cln_l_gbr, i_cln_gbc, i_lst_tpl_MetaCol, \
						i_str_where, i_str_expr_agg )
					pivot_didok = xd2rd_dbi_Execute_Make( p_conn, sql_pivotv, i_cln_p2m)
					if pivot_didok :
						p_dct_ro_pv[ i_cln_mk ] = sql_pivotv
						# generate the INSERT
						sql_insert = xd2rd_RO_GenerateSQL_Insertion( i_cln_t2m, i_cln_p2m, i_cln_idc, i_lst_tpl_MetaCol )
						insert_didok = xd2rd_dbi_Execute( p_conn, sql_insert)
						if insert_didok :
							p_dct_ro_it[ i_cln_mk ] = sql_insert
						else :
							print( "FAIL: Implementing the Generated Insert")
							print( sql_insert )
					else :
						print( "FAIL: Implementing the Generated Pivot")
						print( sql_pivotv )
				else :
					print( "FAIL: Implementing the Generated Create")
					print( sql_create )

# =====================================================
# Stage 4
# ----------------------------------
# Inserts using Structure Info Prep Views

def xd2rd_RO_Populate_StrctInfo_TableIndex( p_conn, p_reload_id ):
	print("xd2rd_RO_Populate_StrctInfo_TableIndex")
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_ti.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_pth + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_tag + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_d + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_pt + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_tn + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_pth + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_tag + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_d + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_pt + ", " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ptn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_StrctInfo_TableIndex.rsn + " " + "\n" + \
		"WHERE " + "\n" + \
		"  " + xd2rd_nt_t_ti.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		";"
	#print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )

def xd2rd_RO_Populate_StrctInfo_TablePathStep( p_conn, p_reload_id ):
	print("xd2rd_RO_Populate_StrctInfo_TablePathStep")
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_tps.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_tn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_sn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_tag + " " + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_tn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_sn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_tag + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_StrctInfo_TablePathStep.rsn + " " + "\n" + \
		"WHERE " + "\n" + \
		"  " + xd2rd_nt_t_tps.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		";"
	#print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )

def xd2rd_RO_Populate_StrctInfo_TableLinkDetail( p_conn, p_reload_id ):
	print("xd2rd_RO_Populate_StrctInfo_TableLinkDetail")
	sql_str = "" + \
		"INSERT INTO " + "\n" + \
		"  " + xd2rd_nt_t_tld.rsn + " " + "\n" + \
		"( " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_tn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_atcn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_tocn + " " + "\n" + \
		"  ) " + "\n" + \
		"SELECT " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_ri + ", " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_tn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_atcn + ", " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_tocn + " " + "\n" + \
		"FROM " + "\n" + \
		"  " + xd2rd_nt_v_StrctInfo_TableLinkDetail.rsn + " " + "\n" + \
		"WHERE " + "\n" + \
		"  " + xd2rd_nt_t_tld.cln_ri + " = " + str( p_reload_id ) + " " + "\n" + \
		";"
	#print( sql_str )
	didok = xd2rd_dbi_Execute( p_conn, sql_str)
	#print( didok )
	if not didok :
		print( sql_str )

def xd2rd_RO_Populate_StructureInfo_Tables( p_conn, p_reload_id ):
	# Call the four population actions for the StructureInfo Tables
	print( "xd2rd_RO_Populate_StructureInfo_Tables")
	xd2rd_RO_Populate_StrctInfo_TableIndex( p_conn, p_reload_id )
	xd2rd_RO_Populate_StrctInfo_TablePathStep( p_conn, p_reload_id )
	#xd2rd_RO_Populate_StrctInfo_TableLink( p_conn, p_reload_id )
	xd2rd_RO_Populate_StrctInfo_TableLinkDetail( p_conn, p_reload_id )

# =====================================================
# Calling the Stages

def xd2rd_RelationalOutput_Constructions( p_conn, p_reload_id, \
		p_lst_ro_tn, p_dct_ro_ct, p_dct_ro_pv, p_dct_ro_it ) :
	# the task here is to go through the sets of tables and columns and construct CREATE TABLE sql for each
	print( "xd2rd_RelationalOutput_Constructions" )
	#
	# Stage 1 - create the Hierarchial Node tables
	# These must be done first to allow the others to apply referential 
	# integrity by referring to these
	print( "Stage 1 - create the Hierarchial Node tables")
	xd2rd_RO_Hierarchial_Generated_Construction( p_conn, p_reload_id, p_lst_ro_tn, p_dct_ro_ct, p_dct_ro_it )
	# Stage 2 - populate the Meta tables
	print( "Stage 2 - populate the Meta tables")
	xd2rd_RO_Populate_Construction_Framework( p_conn, p_reload_id )
	Inspect_Construction_Framework_SpecificReload( p_conn, p_reload_id )
	# Stage 3 - use the Meta tables to generate Relational Output tables
	print( "Stage 3 - use the Meta tables to generate Relational Output tables")
	xd2rd_RO_Meta_Generated_Construction( p_conn, p_reload_id, \
		p_lst_ro_tn, p_dct_ro_ct, p_dct_ro_pv, p_dct_ro_it )
	# Stage 4 - use the Meta tables to annotate Relational Output tables
	print( "Stage 4 - use the Meta tables to annotate Relational Output tables")
	Inspect_MetaRecordSets_SpecificReload_Virtual( p_conn, p_reload_id )
	xd2rd_RO_Populate_StructureInfo_Tables( p_conn, p_reload_id )
	Inspect_MetaRecordSets_SpecificReload_Actual( p_conn, p_reload_id )
	p_lst_ro_tn.append( xd2rd_nt_t_ti.rsn )

# :::::::::::::::::::::::::::::::::::::::::::::::::::::


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Abstracted Data Calls 
# These bridge the Python XML handling with calling the SQLite calls 

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Plain Writes to the base tables

def xd2rd_Data_Write_Data_Meta( p_conn, p_i, p_n):
	xd2rd_insert_genera( p_conn, p_i, p_n )

def xd2rd_Data_Write_Data_Schema( p_conn, p_i, p_mi, p_n, p_v, p_d):
	xd2rd_insert_schema( p_conn, p_i, p_mi, p_n, p_v, p_d )

def xd2rd_Data_Write_Data_Fount( p_conn, p_i, p_n, p_f, p_dts):
	xd2rd_insert_fount( p_conn, p_i, p_n, p_f, p_dts )

def xd2rd_Data_Write_Data_Reload( p_conn, p_i, p_si, p_rt, p_fi ):
	xd2rd_insert_reload( p_conn, p_i, p_si, p_rt, p_fi )

def xd2rd_Data_Write_Data_Node( p_conn, p_ri, p_ni, p_oi, p_d, p_t, p_c, p_lst_ids, p_lst_tags):
	# convert lists to text
	i_ps = ConvertTagListToTableName( p_lst_tags )
	# - id list
	isd_j = EncodeListToJson( p_lst_ids )
	# isu_l = list(tsd.reverse)
	isu_l = p_lst_ids[::-1]
	isu_j = EncodeListToJson( isu_l )
	# - tag list
	tsd_j = EncodeListToJson( p_lst_tags )
	tsu_l = p_lst_tags[::-1]
	tsu_j = EncodeListToJson( tsu_l )
	# context
	i_lst_tags = p_lst_tags.copy()
	i_lst_tags.append( p_t)
	i_ctx = EncodeListToJson( i_lst_tags )
	# submit
	xd2rd_insert_node( p_conn, p_ri, p_ni, p_oi, p_d, p_t, p_c, i_ps )
  # xd2rd_insert_node( p_conn, p_ri, p_i, p_oi, p_d, p_t, p_c, p_isd, p_isu, p_ps, p_tsd, p_tsu, p_ctx )
  
def xd2rd_Data_Write_Data_Attr( p_conn, p_ri, p_ni, p_k, p_v):
	xd2rd_insert_attr( p_conn, p_ri, p_ni, p_k, p_v )

def xd2rd_Data_Write_Data_MetaTable( p_conn, p_ri, p_i, p_tsd, p_n, p_fti, p_m):
	xd2rd_insert_metatable( p_conn, p_ri, p_i, p_tsd, p_n, p_fti, p_m )

def xd2rd_Data_Write_Data_MetaColumn( p_conn, p_ri, p_i, p_ti, p_n, p_m):
	xd2rd_insert_metacolumn( p_conn, p_ri, p_i, p_ti, p_n, p_m )

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

def xd2rd_Data_Show_Data_Genera( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_g.rsn )

def xd2rd_Data_Show_Data_Schema( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_s.rsn )

def xd2rd_Data_Show_Data_Fount( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_f.rsn )

def xd2rd_Data_Show_Data_Reload( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_r.rsn )

def xd2rd_Data_Show_Data_Node( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_n.rsn )

def xd2rd_Data_Show_Data_Attr( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_a.rsn )

def xd2rd_Data_Show_Data_MetaTable( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_t.rsn )

def xd2rd_Data_Show_Data_MetaColumn( p_conn):
	xd2rd_Inspect_Rst_Limit( p_conn, xd2rd_nt_t_c.rsn )

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Python XML handling

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Sketched function for later detecting Schema references
# Not yet used

def xd2rd_Scan_XML_for_XSD( p_conn, e):
	i = 0
	while i < 20 :
		elmnt_tag = e.tag
		if elmnt_tag == "root" :
			for a_k in e.attrib.keys():
				if a_k == "xsi:schemaLocation" :
					print("Schema Reference BINGO !")
					a_v = e.attrib.get( a_k, "")
					p_i = 1 
					p_mi = 0 
					p_n = a_v
					p_v = ""
					p_d = 0
					xd2rd_Data_Write_Data_Schema( p_conn, p_i, p_mi, p_n, p_v, p_d)
		i = i + 1

def xd2rd_Traverse_XML_And_Load( p_conn, e, p_ri, p_ni, p_oi, p_depth, p_lst_ids, p_lst_tags):
	# print("xd2rd_Traverse_XML_And_Load" + " p_ni:" + str(p_ni))
	# increment for a new node ID and new depth
	i_depth = p_depth + 1
	i_ni = p_ni + 1
	i_upto_node_id = i_ni
	# get from element 
	# print( e.tag)
	# print( e.text)
	if False :
		elmnt_tag = e.tag
	else :
		throwaway_a, throwaway_b, elmnt_tag = (e.tag).rpartition("}")
	if e.text is None :
		elmnt_val = ""
	else :
		elmnt_val = e.text.strip()
	xd2rd_Data_Write_Data_Node( p_conn, p_ri, i_ni, p_oi, i_depth, elmnt_tag, elmnt_val, p_lst_ids, p_lst_tags)
	# do any attributes
	# print( "Attributes")
	for a_k in e.attrib.keys():
		a_v = e.attrib.get( a_k, "")
		xd2rd_Data_Write_Data_Attr( p_conn, p_ri, i_ni, a_k, a_v)
		# print( "Node: " + str(i_ni) + " A: Key: " + a_k + " Value: " + a_v)
	# do any sub-elements
	# make new lists to pass to sub-elements
	i_lst_ids = p_lst_ids.copy()
	i_lst_ids.append( i_ni )
	i_lst_tags = p_lst_tags.copy()
	i_lst_tags.append( elmnt_tag )
	for sub_e in e:
		i_upto_node_id = xd2rd_Traverse_XML_And_Load( p_conn, sub_e, p_ri, i_upto_node_id, i_ni, i_depth, i_lst_ids, i_lst_tags)
	return i_upto_node_id

def xd2rd_Process_XML_File_Into_Sqlite_Connection( p_conn, xml_fileref, p_run_id, p_node_id):
	print("xd2rd_Process_XML_File_Into_Sqlite_Connection")
	print("File: " + xml_fileref )
	print("p_run_id: " + str(p_run_id) )
	print("p_node_id: " + str(p_node_id) )
	tree = ET.parse( xml_fileref)
	root = tree.getroot()
	xd2rd_Scan_XML_for_XSD( p_conn, root)
	i_owner_id = p_node_id
	i_depth = 0
	i_lst_tags = [] # start a new tag list
	i_lst_ids = [] # start a new node ID list
	upto_node_id = xd2rd_Traverse_XML_And_Load( p_conn, root, p_run_id, p_node_id, i_owner_id, i_depth, i_lst_ids, i_lst_tags)
	return upto_node_id

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Application processes

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# Result Inspections


def Outlog_Recordset( p_conn, rst_name ) :
	# 
	v_fetch_sql = "SELECT * FROM " + rst_name # + " LIMIT 200 ;" 
	xd2rd_dbi_Outlog_Title( p_conn, v_fetch_sql, rst_name )

def Display_Lst_Of_Strings( p_lst ) :
	for k in p_lst :
		print( "Table: " + k )

def Display_Dct_Of_Strings( p_dct ) :
	for k in p_dct.keys() :
		print( "Dictionary Key: " + k + " (next line is value at ths key.")
		print( p_dct[k] )

def RelationalOutput_Inspections( p_conn, \
		p_lst_ro_tn, p_dct_ro_ct, p_dct_ro_pv, p_dct_ro_it ) :
	# Inspect all the constructions 
	#print( "Display Relational Output Tables and the TagSequences they represent:" )
	#Display_Lst_Of_Strings( p_lst_ro_tn )
	#print( "Display SQL for Constructing Relational Output Tables:" )
	#Display_Dct_Of_Strings( p_dct_ro_ct )
	#print( "Display SQL for Population Views for the Relational Output Tables:" )
	#Display_Dct_Of_Strings( p_dct_ro_pv )
	#print( "Display SQL for Insertions into the Relational Output Tables:" )
	#Display_Dct_Of_Strings( p_dct_ro_it )
	print( "Display contents of the generated Relational Output Tables:" )
	for tbl_name in p_lst_ro_tn :
		print( "Table: " + tbl_name )
		Outlog_Recordset( p_conn, tbl_name )

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# XML loading, post load analysis and table generation

# Process just one file

def Process_XML_File_Into_Sqlite( use_xfn, use_i_f, use_i_r, use_i_n, p_do_file, p_pathfilename ) :
	print("Process_XML_File_Into_Sqlite")
	print("File: " + use_xfn )
	print("use_i_f: " + str(use_i_f) )
	print("use_i_r: " + str(use_i_r) )
	print("use_i_n: " + str(use_i_n) )
	got_to_n = use_i_n
	didok, conn = xd2rd_setup( p_do_file, p_pathfilename ) # "x2r.db"
	if didok :
		print("Setup successful!")
		# clear the result output log 
		Hack_Result_TblLog_Clear()
		# Step 1 Load from XML
		got_to_n = xd2rd_Process_XML_File_Into_Sqlite_Connection( conn, use_xfn, use_i_r, use_i_n )
		print( "got_to_n:" + str(got_to_n) )
		Inspect_RecordSets_SpecificReload( conn, use_i_r )
		# do the Relatonal Output (ro) phases
		i_lst_tables_made = []
		i_dct_ro_sql_make_tables = {}
		i_dct_ro_sql_make_views = {}
		i_dct_ro_sql_insert_tables = {}
		# Step 1 Generate Data-driven Tables
		xd2rd_RelationalOutput_Constructions( conn, use_i_r, \
			i_lst_tables_made, i_dct_ro_sql_make_tables, i_dct_ro_sql_make_views, i_dct_ro_sql_insert_tables)
		RelationalOutput_Inspections( conn, \
			i_lst_tables_made, i_dct_ro_sql_make_tables, i_dct_ro_sql_make_views, i_dct_ro_sql_insert_tables )
		# Close the database
		didok = sqlite_close_connection( conn )
		if didok :
			print("Closed database successfully!")
	else :
		print("Setup failed!")
	return got_to_n

# Process a list of files
# To show a simple use of noting where the Node ID is up to

def Process_XML_File_List( lst_fref ) :
	print("Process_XML_File_List")
	didok, conn = xd2rd_setup()
	if didok :
		print("Setup successful!")
		# clear the result output log 
		Hack_Result_TblLog_Clear()
		use_i_f = 1
		use_i_r = 0
		use_i_n = 1
		i_lst_tables_made = []
		i_dct_ro_sql_make_tables = {}
		i_dct_ro_sql_make_views = {}
		i_dct_ro_sql_insert_tables = {}
		for fref in lst_fref :
			print( "Processing:" + fref )
			use_i_r = use_i_r + 1
			used_i_n = xd2rd_Process_XML_File_Into_Sqlite_Connection( conn, fref, use_i_r, use_i_n )
			print( "got_to_n:" + str(used_i_n) )
			use_i_n = used_i_n + 1
			Inspect_RecordSets_SpecificReload( conn, use_i_r )
			# do the Relatonal Output (ro) phases
			xd2rd_RelationalOutput_Constructions( conn, use_i_r, \
				i_lst_tables_made, i_dct_ro_sql_make_tables, i_dct_ro_sql_make_views, i_dct_ro_sql_insert_tables)
		RelationalOutput_Inspections( conn, \
			i_lst_tables_made, i_dct_ro_sql_make_tables, i_dct_ro_sql_make_views, i_dct_ro_sql_insert_tables )
		# Close the database
		didok = sqlite_close_connection( conn )
		if didok :
			print("Closed database successfully!")
	else :
		print("Setup failed!")


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# :::::::::::::::::::::::::::::::::::::::::::::::::::::

# main only if run as a program
if __name__ == "__main__":
	testmode = ""
	if True:
		print( 'Number of arguments:', len(im_sys.argv), 'arguments.')
		print( 'Argument List:', str(im_sys.argv))
		arg_count = len(im_sys.argv)
		if arg_count < 2 :
			testmode = input ("Sorry, must supply a path. Enter a command or just Press <Enter> for a preset test run.")
			exit
		else:
			testmode = "nope"
			use_xfn = im_sys.argv[1]
			Process_XML_File_Into_Sqlite( use_xfn, 1, 1, 1, False, "x2r.db") # process into memory SQlite
	if testmode == 'list' :
		lst_frefs = []
		lst_frefs.append( "/home/ger/Downloads/simple.xml")
		lst_frefs.append( "/home/ger/Downloads/note.xml")
		lst_frefs.append( "/home/ger/Downloads/cd_catalog.xml")
		lst_frefs.append( "/home/ger/Downloads/plant_catalog.xml")
		lst_frefs.append( "/home/ger/ger/.local/share/JetBrains/PyCharmCE2020.1/availables.xml")
		Process_XML_File_List( lst_frefs)
	elif len(testmode) == 0 :
		use_xfn = "/home/ger/Documents/dev/xd2rd/mock.xml"
		Process_XML_File_Into_Sqlite( use_xfn, 1, 1, 1)
	print( "Completed!")


# attach database 'c:\sqlite\db\contacts.db' as contacts;
