# XD2RD

XML Data to Relational Data

A tool for automated translation of data from XML to relational data tables. 

## Current status

Currrent status: alpha release - as a working proof of the method.

## Why write this?

The key purpose here is full automation for any feed of XML, not just those already known.

There do exist various tools and code for handling an XML feed either:
- because the structure is already understood;
- to generate code for handling a given (specific) structure.

It has been written for use with Python 3 and the SQLite library module, but this is merely the first implementation. The intention is to apply the concept to whichever platform and language mix is required (see license notes below).

The author wrote this program from not being able to find anything to do what was wanted available as Open Source. If someone else had done so, then they wouldn't have.

## Brief

XD2RD reads an XML and writes to two holding tables. Then it uses relational logic to determine three sets of tables to create - i.e. as a relational output set.

Those sets are:
- tables to hold the XML attributes - one table per structure position
- tables to hold the XML elements that are unique per structure position and have no child nodes - one table per structure position
- tables to hold all the other XML elements - one table per structure position

Where a "structure position" is a place in the hierarchy of XML tags.

How this works out is mostly dependent on the XML structures encountered.

The primary purpose is to achieve a valid translation. Perhaps later, it will concern itself with various quality improvements, e.g.
- storage efficiency;
- query response times;
- and other optimisations.

A second intended phase will be to use Schema (XSD) references to guide the translation. The main reason for that would be to provide stabiity when faced by data not utilising all the features in a schema - i.e. so as not produce a different structure when known-to-be-possible data eventually turns up.
It is not intended that this tool will ever test XML data for compliance with a schema - it will be presumed that if data quotes a schema then it complies with the schema.

At any rate, schema guided features will not be added until the existing code has settled into a beta development phase.

## Software License

XD2RD is Free Open Source Software (FOSS) using the GPL3 license.

As a note to those who may find that concerning, let me be clear:
- the author has followed the evolution of FOSS since its birth and is well versed in the concepts;
- yes, I know that GPL is not the norm for Python applications;
- Copyright - and thus the license - applies to my specific code, so a fork of this source code has to also respect and apply the GPL;
- FOSS means you are free to study the code and use any concepts you see to write your own software, where you can thereby choose a license. No need to ask, please go ahead and do that (but see below about naming).

## Naming

Copyright (and thus the chosen license) doesn't really cover the titles of things; such is usually a matter of "trademark" and other legal concepts, which vary considerably around the world. The names used here were chosen to be distinct, so re-using the top-most name "Foldatry" without an agreed arrangement will just confuse people. So maybe don't do that. Alas, the way that trademark laws work usually favours whoever pays money for registering a trademark, so unless/until this project gains some patronage that won't be happening.

In short, ruthless actors can make life difficult, and this text is only here to make it clear what the answer would be to AITA questions from any that do so.

The name of the project is not an important feature.

## Data Structure

The initial concept for XD2RD calls for seven stable tables.

There are three organisation tables - although for the alpha code upload, there are not yet in use.

- Genera = a set of versioned sequenced Schemas
- Schma = Schema - a quoted or derived XML Schema, if of a version sequence should know its predecessor
- Reload = a loading run, quoting source, when it was done, what Schema it had

The XML data is converted using essentially Python code to populate two tables:

- Node = with a row for each XML element (the thing between tags)
- Attribute = unique named values in inside an element (in the form of AttributeName=AttributeValue)

For development convenience, there a two tables that acts translation control lists. These could eventually be replaced by the views that populate them, but of now it is convenient to be able to inspect them during and after processing.

- MetaTable
- MetaColumn

There is then:
- a stock set of views and processes to bring out feature information of the deserialised data

Finally, for each distinct XML schema or file read, there is:
- a generated set of view and processes - being a set per structural variation

For the alpha code release, as there is yet no super-organisational handling (Genera, Schema) the distinct result table naming is enfored by using an ID generated per source XML file. This will later be changed to quote the Schema ID.

The alpha release uses an in-memory SQLite database - hence it creates all the required structures for each run, populates them, outputs and then loses it all. This is an obviously temporary arrangement and will need to be replaced by suitable layers of database and file bureaucracy.

Similarly, the alpha release - if given a list of XML files to act on - uses a single datbase for the load, processing and all result views and tables. This will also be replaced by more complex database and file options - e.g. a separate SQLite database file per result File or Schema.

## Plans

For now, the code has been released for study and adaptation. There is not yet any specific roadmap. It may even be that this is all that was necessary if implementions of the concept are done in other projects.

## Addtional Code Comments

The author of the present code is not highly skilled in Python, which may explain some curious aspects. In short, getting the code to be a functional proof the general translation method has been a higher priority than any Pythonesqe niceties. Suggestions for making it more Python-normal are welcome.

There was a deliberate intention to use as few Python libraries as possible for the alpha release. This was due to an expectation that some interest would come from users in highly restricted environments where the request for access to an addtional library can be a hurdle to even testing the code. With the alpha established, this need not be a requirement going forward.

That's a long way to explain the use of named tuples as the naming abtraction. Named tuples were used merely because:
- the author was familiar with them;
- they are immutable, so effectively usable as constants
- allowed a simple way to abstract the SQL object naming from the Python code names.

As an intended target for this program is potential use with extremely large amounts of data, the principle applied is to do things in SQL where ever possble rather than in Python. Thus it is the relational engine that handles the scale, with Python merely acting as an automation framing device (something SQL lacks as a declaratvie language).

Despite that, no attempt has been made to utilise Stored Procedures as it is not reliably supported across data engines, including those handling very large scale (such as Hadoop).

An obvious aspect of the alpha version that does not scale is that it performs the serial processing of XML in Python. For the author's use cases that is not a significant problem, so the initial design does neither attempts nor imposes any particular way of tackling that.

## Similar solutions

For alpha release, the author has only limited means for checking how this method compares to others that may already exist.

A cursory Internet document search suggests that some academic work has explored similar territory but as the few instances that searching brought up were all locked behind paywalls, no actual inspection of them was feasible.

As a creator of Open Source software, searching was restricted to looking for source code or descriptions of methods, so the existence of this alpha release says nothing at all about what may exist as proprietary software.
